# Description
# ==============================================================================
## Stack local datasets and prepare it for primary analysis: check variables,
## set implausible values to missing, transform categorised numeric variables
## back to numeric forms, sort out any issues with the common data model
## definition or data pre-processing.
##
## Additional non-CDM predictor variables
## - Baseline_type -- MI (acute MI period) and MI40d (after 40 days from MI)
## - Endpoint_type -- SCD (including VA and SCA), FAT, and FAS
## - IsSWHR -- Swedish Heart Registry indicator
## - CVD_risk_region -- regions with different degrees of risk of CVD

rm(list = ls())

options(width = 112, length = 99999)

# Requirements
# ==============================================================================
library(data.table)
library(ggplot2)
library(ggpubr)

# Input data -- datasets standardised to the common data model
# ==============================================================================
(cdm.files <- list.files("../input", full.names = TRUE))
length(cdm.files)

datasets <- list()
for (f in cdm.files) {
  ## File prefix should be a four-lettered abbreviation of a dataset name
  dbname <- gsub("(.*)-common-data-model.rds", "\\1", basename(f))
  dbname <- toupper(dbname)
  stopifnot(!dbname %in% names(datasets))
  datasets[[dbname]] <- readRDS(f)[, DB := dbname]
}
rm(f, dbname)

names(datasets)
length(datasets)

## Check dimensions
lapply(datasets, dim)

## Stack the datasets
dt <- rbindlist(datasets, fill = TRUE)
dim(dt)

dt[, .N, DB][order(N)]

setcolorder(dt, c("DB", "ID", "Status", "Survival_time"))

## Add dataset name as prefix to ID
dt[, ID := paste(DB, ID, sep = "_")]
stopifnot(nrow(dt) == uniqueN(dt[, ID]))

## Combine PROSE datasets. This has to come after adding DB names as a prefix to
## ID so that we can still distinguish between PRSI and PRSL patients if needed
dt[DB %in% c("PRSI", "PRSL"), .N]
dt[DB %in% c("PRSI", "PRSL"), DB := "PRSE"]

## Drop TRIG
dt <- dt[DB != "TRIG"]

dt[, .N, DB][order(N)]

## Backup
dt.bak <- copy(dt)

## Clean up
rm(datasets, cdm.files)

# Un-categorise categorised numerical variables
# ==============================================================================
## Match by age category and sex, where possible, and just by category and sex
## where age is missing. There is only one patient with missing sex in the whole
## dataset; this patient's age is also missing. Stratify by whether the
## measurements were taken during the acute-MI phase or not.

dt[is.na(Sex), .N, .(DB, ID, Age, Sex)]

## Baseline type:
##  MI -- acute-MI phase
##  MI40d -- any time after 40 days from most recent MI
dt[, .N, Baseline_type]
stopifnot(!anyNA(dt$Baseline_type))

## Numeric variables that were provided as a categorical variable
for (x in colnames(dt)) {
  if (grepl("_cat$", x)) {
    print(x)
    print(dt[, .N, .(DB, cat = !is.na(get(x)))][cat == TRUE])
  }
}
rm(x)

## Provide age quantiles
AgeQuantiles <- function(d) {
  qnt <- quantile(d$Age, probs = c(0, 0.25, 0.5, 0.75, 1), na.rm = TRUE)
  qnt[1] <- 18
  qnt[length(qnt)] <- Inf
  qnt
}

# Convert categorical eGFR into numeric (in FREN only)
# ------------------------------------------------------------------------------
dt[, .N, keyby = .(DB, cat = !is.na(eGFR_cat))][cat == TRUE]

fren <- dt[DB == "FREN", .(ID, Age, Sex, eGFR_cat)]
fren[, .N, keyby = eGFR_cat]
fren[, .N]

dt[DB == "FREN", .N, Baseline_type]

## Reference values to get the medians from
dtref <- dt[DB != "FREN" & Baseline_type == "MI40d", .(Age, Sex, eGFR)]
dtref[, .N]

summary(dtref$eGFR)

## Categorise Age into quantiles
(age.qnt <- AgeQuantiles(dtref))

dtref[, Age_cat := cut(Age, breaks = age.qnt, include.lowest = TRUE)]
dtref[, .N, keyby = Age_cat]

fren[, Age_cat := cut(Age, breaks = age.qnt, include.lowest = TRUE)]
fren[, .N, keyby = Age_cat]

dtref[, eGFR_cat := NA_character_]
dtref[eGFR < 30, eGFR_cat := "< 30 ml/min"]
dtref[eGFR >= 30 & eGFR <= 60, eGFR_cat := "30-60 ml/min"]
dtref[eGFR > 60, eGFR_cat := "> 60 ml/min"]

## By eGFR category, age and sex
(egfr.age.sex <- dtref[, .(eGFR_median = median(eGFR, na.rm = TRUE)),
                       keyby = .(Age_cat, Sex, eGFR_cat)])
(egfr.age.sex <- na.omit(egfr.age.sex))

fren <- merge(fren, egfr.age.sex,
              by = c("Age_cat", "Sex", "eGFR_cat"), all.x = TRUE)
setnames(fren, "eGFR_median", "eGFR")

## By eGFR category and sex if age was missing
(egfr.sex <- dtref[, .(eGFR_median = median(eGFR, na.rm = TRUE)),
                   keyby = .(Sex, eGFR_cat)])
(egfr.sex <- na.omit(egfr.sex))

fren <- merge(fren, egfr.sex, by = c("Sex", "eGFR_cat"), all.x = TRUE)
fren[!is.na(eGFR_cat) & is.na(eGFR)]

fren[!is.na(eGFR_cat) & is.na(eGFR), eGFR := eGFR_median]

## Merge into the main data.table
stopifnot(identical(sort(dt[DB == "FREN", ID]), sort(fren$ID)))
dt <- merge(dt, fren[, .(ID, eGFR_fren = eGFR)],
            by = "ID", all.x = TRUE, sort = FALSE)

dt[DB == "FREN", eGFR := eGFR_fren]

suppressWarnings(
  dt[DB == "FREN", .(min = min(eGFR, na.rm = TRUE),
                     median = median(eGFR, na.rm = TRUE),
                     max = max(eGFR, na.rm = TRUE)),
    keyby = eGFR_cat]
)

dt[, c("eGFR_cat", "eGFR_fren") := NULL]

## Clean up
rm(age.qnt, egfr.age.sex, egfr.sex, fren, dtref)

# Convert categorical QRS into numeric (FREN and DOIT)
# ------------------------------------------------------------------------------
dt[, .N, keyby = .(DB, cat = !is.na(QRS_cat))][cat == TRUE]

fren.doit <- dt[DB %in%  c("FREN", "DOIT"),
                .(DB, ID, Age, Sex, Baseline_type, QRS_cat)]
fren.doit[, .N, keyby = .(QRS_cat, DB)]
fren.doit[, .N]

dt[DB %in%  c("FREN", "DOIT"), .N, Baseline_type]

dtref <- dt[!DB %in%  c("FREN", "DOIT"), .(Age, Sex, eGFR, QRS, Baseline_type)]
dtref[, .N]

dtref[, QRS_cat := NA_character_]
dtref[QRS < 120, QRS_cat := "<120"]
dtref[QRS >= 120 & QRS <= 150, QRS_cat := "120-150"]
dtref[QRS > 150, QRS_cat := ">150"]

## Drop QRS < 50
dtref <- dtref[QRS >= 50]
dim(dtref)

summary(dtref$QRS)

dtref[, .N, Baseline_type]

## Categorise Age into quantiles
(age.qnt <- AgeQuantiles(dtref))

dtref[, Age_cat := cut(Age, breaks = age.qnt, include.lowest = TRUE)]
dtref[, .N, keyby = Age_cat]

fren.doit[, Age_cat := cut(Age, breaks = age.qnt, include.lowest = TRUE)]
fren.doit[, .N, keyby = Age_cat]

## By category, age, sex and baseline type
(qrs.age.sex <- dtref[, .(QRS_median = median(QRS, na.rm = TRUE)),
                      keyby = .(Baseline_type, Age_cat, Sex, QRS_cat)])
(qrs.age.sex <- na.omit(qrs.age.sex))

fren.doit <- merge(fren.doit, qrs.age.sex,
                   by = c("Baseline_type", "Age_cat", "Sex", "QRS_cat"),
                   all.x = TRUE)
setnames(fren.doit, "QRS_median", "QRS")

## By category, sex and baseline type if age was missing
(qrs.sex <- dtref[, .(QRS_median = median(QRS, na.rm = TRUE)),
                  keyby = .(Baseline_type, Sex, QRS_cat)])
(qrs.sex <- na.omit(qrs.sex))

fren.doit <- merge(fren.doit, qrs.sex,
                   by = c("Baseline_type", "Sex", "QRS_cat"), all.x = TRUE)
fren.doit[!is.na(QRS_cat) & is.na(QRS)]

fren.doit[!is.na(QRS_cat) & is.na(QRS), QRS := QRS_median]
fren.doit

## Merge into the main data.table
stopifnot(identical(sort(dt[DB %in%  c("FREN", "DOIT"), ID]), sort(fren.doit$ID)))

dt <- merge(dt, fren.doit[, .(ID, QRS_fren_doit = QRS)],
            by = "ID", all.x = TRUE, sort = FALSE)

dt[DB %in% c("FREN", "DOIT"), QRS := QRS_fren_doit]

suppressWarnings(
  dt[DB %in% c("FREN", "DOIT"), .(min = min(QRS, na.rm = TRUE),
                                  median = median(QRS, na.rm = TRUE),
                                  max = max(QRS, na.rm = TRUE)),
     keyby = QRS_cat]
)

dt[, c("QRS_cat", "QRS_fren_doit") := NULL]

## Clean up
rm(age.qnt, qrs.age.sex, qrs.sex, fren.doit, dtref)

# Convert categorical Troponin_T values into numeric (in HELS only)
# ------------------------------------------------------------------------------
## Troponin_T is set to missing at acute-phase
suppressWarnings(
  dt[!is.na(Troponin_T_cat)][is.na(as.numeric(Troponin_T_cat))][, .N, .(DB, Troponin_T_cat)]
)

dt[DB == "HELS", .N, Baseline_type]

dt[DB == "HELS" & Troponin_T_cat != "<100", Troponin_T := as.numeric(Troponin_T_cat)]

dtref <- dt[Troponin_T < 100 & Baseline_type == "MI40d"]
dim(dtref)

summary(dtref$Troponin_T)

(age.qnt <- AgeQuantiles(dtref))

dtref[, Age_cat := cut(Age, breaks = age.qnt, include.lowest = TRUE)]
dtref[, .N, keyby = Age_cat]

tt.100 <- dt[Troponin_T_cat == "<100", .(ID, Age, Sex, Troponin_T_cat)]
tt.100[, Age_cat := cut(Age, breaks = age.qnt, include.lowest = TRUE)]
tt.100[, .N, keyby = Age_cat]

tt <- dtref[, .(TT_median = median(Troponin_T, na.rm = TRUE)), .(Age_cat, Sex)]

tt.100 <- merge(tt.100, tt, by = c("Age_cat", "Sex"))
tt.100

## Merge into the main data.table
dt <- merge(dt, tt.100[, .(ID, TT_100 = TT_median)], by = "ID",
            all.x = TRUE, sort = FALSE)

dt[Troponin_T_cat == "<100", Troponin_T := TT_100]
dt[Troponin_T_cat == "<100", .(ID, Troponin_T)]

dt[, Troponin_T_cat := NULL]
dt[, TT_100 := NULL]

suppressWarnings(
  dt[, .(Min = min(Troponin_T, na.rm = TRUE),
         Mean = mean(Troponin_T, na.rm = TRUE),
         Median = median(Troponin_T, na.rm = TRUE),
         Max = max(Troponin_T, na.rm = TRUE),
         NA_prop = round(sum(is.na(Troponin_T)) / .N, 2)),
     DB]
)

## Clean up
rm(age.qnt, dtref, tt, tt.100)

# Convert categorical CRP values into numeric (in HELS only)
# ------------------------------------------------------------------------------
## CRP is set to missing at acute-phase
suppressWarnings(
  dt[!is.na(CRP_cat)][is.na(as.numeric(CRP_cat))][, .N, .(DB, CRP_cat)]
)

dt[DB == "HELS" & CRP_cat != "<0.3", CRP := as.numeric(CRP_cat)]

dtref <- dt[CRP < 0.3 & Baseline_type == "MI40d"]
dim(dtref)

summary(dtref$CRP)

(age.qnt <- AgeQuantiles(dtref))

dtref[, Age_cat := cut(Age, breaks = age.qnt, include.lowest = TRUE)]
dtref[, .N, keyby = Age_cat]

crp.3 <- dt[CRP_cat == "<0.3", .(ID, Age, Sex, CRP_cat)]
crp.3[, Age_cat := cut(Age, breaks = age.qnt, include.lowest = TRUE)]
crp.3

crp <- dtref[, .(CRP_median = median(CRP, na.rm = TRUE)), .(Age_cat, Sex)]
crp

crp.3 <- merge(crp.3, crp, by = c("Age_cat", "Sex"))
crp.3

## Merge into the main data.table
dt <- merge(dt, crp.3[, .(ID, CRP_3 = CRP_median)], by = "ID",
            all.x = TRUE, sort = FALSE)

dt[CRP_cat == "<0.3", CRP := CRP_3]
dt[CRP_cat == "<0.3", .(ID, CRP_cat, CRP)]

dt[, CRP_cat := NULL]
dt[, CRP_3 := NULL]

suppressWarnings(
  dt[, .(Min = min(CRP, na.rm = TRUE),
         Mean = mean(CRP, na.rm = TRUE),
         Median = median(CRP, na.rm = TRUE),
         Max = max(CRP, na.rm = TRUE),
         NA_prop = round(sum(is.na(CRP)) / .N, 2)),
     DB]
)

## Clean up
rm(age.qnt, dtref, crp, crp.3)

# Convert diseased number of arteries into categorical variable
# ------------------------------------------------------------------------------
dt[, .N, keyby = .(DB, Diseased_arteries_num)][!is.na(Diseased_arteries_num)]
dt[, .N, keyby = .(DB, Diseased_arteries_num_cat)][!is.na(Diseased_arteries_num_cat)]

dt[Diseased_arteries_num_cat == "3 and above",
   Diseased_arteries_num_cat := "geq3"]

dt[is.na(Diseased_arteries_num_cat) & !is.na(Diseased_arteries_num),
   Diseased_arteries_num_cat := as.character(Diseased_arteries_num)]

dt[Diseased_arteries_num_cat %in%  c("3", "4"),
   Diseased_arteries_num_cat := "geq3"]

dt[, .N, keyby = .(Diseased_arteries_num, Diseased_arteries_num_cat)]

dt[, .N, keyby = .(DB, Diseased_arteries_num, Diseased_arteries_num_cat)]

dt[, Diseased_arteries_num_cat :=
       factor(Diseased_arteries_num_cat, levels = c("0", "1", "2", "geq3"),
              ordered = TRUE)]

## Clean up
dt[, Diseased_arteries_num := NULL]

# Wrap up
# ------------------------------------------------------------------------------
setcolorder(dt, c("DB", "ID", "Status", "Survival_time"))
stopifnot(identical(sort(dt.bak$ID), sort(dt$ID)))

x.dropped <- c("eGFR_cat", "QRS_cat", "CRP_cat", "Troponin_T_cat",
               "Diseased_arteries_num")

stopifnot(identical(
  sort(names(dt.bak[, -..x.dropped])), sort(names(dt))
))

## Clean up
rm(x.dropped, AgeQuantiles)

# Variable checks
# ==============================================================================
SummaryNum <- function(dt, x, plot = TRUE) {
  print(dt[, .(Min = min(get(x), na.rm = TRUE),
               Mean = mean(get(x), na.rm = TRUE),
               Median = median(get(x), na.rm = TRUE),
               Max = max(get(x), na.rm = TRUE),
               NA_count = sum(is.na(get(x))),
               NA_prop = round(sum(is.na(get(x))) / .N, 2)),
           DB])
  print(summary(dt[[x]]))
  ## if (plot) boxplot(dt[[x]] ~ dt[["DB"]], cex.axis = 0.7, ylab = x, xlab = "DB")
}

SummaryCat <- function(dt, x) {
  print(dt[, .N, keyby = .(DB, get(x))])
  print(table(dt[[x]], useNA = "always"))
}

GetUpperLim <- function(dt, x, k = 7) {
  iqr <- IQR(dt[[x]], na.rm = TRUE)
  uq <- quantile(dt[[x]], probs = 0.75, na.rm = TRUE)[[1]]
  uq + k * iqr
}

# Age
# ------------------------------------------------------------------------------
SummaryNum(dt, "Age")

stopifnot(all(dt$Age >= 18, na.rm = TRUE))

# Sex
# ------------------------------------------------------------------------------
SummaryCat(dt, "Sex")

# Ethnicity
# ------------------------------------------------------------------------------
dt[, Ethnicity := NULL]

# BMI
# ------------------------------------------------------------------------------
SummaryNum(dt, "BMI")

dt[BMI < 12, BMI := NA]

GetUpperLim(dt, "BMI")

dt[BMI > 69, BMI := NA]

# SBP
# ------------------------------------------------------------------------------
SummaryNum(dt, "SBP")

dt[Baseline_type == "MI", SBP := NA]

## Exclude one patient as per data provider's advice
dt[DB == "HELS" & SBP < 57, SBP := NA]

# DBP
# ------------------------------------------------------------------------------
SummaryNum(dt, "DBP")

dt[Baseline_type == "MI", DBP := NA]

## Exclude one patient as per data provider's advice
dt[DB == "HELS" & DBP < 11, DBP := NA]

# NYHA
# ------------------------------------------------------------------------------
SummaryCat(dt, "NYHA")

dt[Baseline_type == "MI", NYHA := NA]

# MI_type
# ------------------------------------------------------------------------------
SummaryCat(dt, "MI_type")

# MI_location_anterior
# ------------------------------------------------------------------------------
SummaryCat(dt, "MI_location_anterior")

# MI_location_inferior
# ------------------------------------------------------------------------------
SummaryCat(dt, "MI_location_inferior")

# MI_location_lateral
# ------------------------------------------------------------------------------
SummaryCat(dt, "MI_location_lateral")

# MI_location_posterior
# ------------------------------------------------------------------------------
SummaryCat(dt, "MI_location_posterior")

# PCI
# ------------------------------------------------------------------------------
SummaryCat(dt, "PCI")

# CABG
# ------------------------------------------------------------------------------
SummaryCat(dt, "CABG")

dt[, CABG := droplevels(CABG)]

# PCI_acute
# ------------------------------------------------------------------------------
SummaryCat(dt, "PCI_acute")

# CABG_acute
# ------------------------------------------------------------------------------
SummaryCat(dt, "CABG_acute")

# Thrombolysis_acute
# ------------------------------------------------------------------------------
SummaryCat(dt, "Thrombolysis_acute")

# Revascularisation_acute
# ------------------------------------------------------------------------------
SummaryCat(dt, "Revascularisation_acute")

dt[PCI_acute == "Yes" | CABG_acute == "Yes" | Thrombolysis_acute == "Yes",
   Revascularisation_acute := "Yes"]

dt[PCI_acute == "No" & CABG_acute == "No" & Thrombolysis_acute == "No",
   Revascularisation_acute := "No"]

dt[PCI_acute == "No" & is.na(CABG_acute) & Thrombolysis_acute == "No",
   Revascularisation_acute := "No"]

dt[, .N, keyb = .(PCI_acute, CABG_acute, Thrombolysis_acute,
                  Revascularisation_acute)]

dt[, .N, keyb = .(PCI_acute, CABG_acute, Thrombolysis_acute,
                  Revascularisation_acute, DB)]

# Diseased_arteries
# ------------------------------------------------------------------------------
SummaryCat(dt, "Diseased_arteries_num_cat")

# LVH
# ------------------------------------------------------------------------------
dt[, LVH := NULL]

# LVEF
# ------------------------------------------------------------------------------
SummaryNum(dt, "LVEF")

# Use MRI LVEF when it's available and echo LVEF is missing
dt[is.na(LVEF) & !is.na(MRI_LVEF), .N,
   keyby = .(DB, mri.lvef.na = is.na(MRI_LVEF))]

dt[is.na(LVEF) & !is.na(MRI_LVEF), LVEF := MRI_LVEF]

# LVDD
# ------------------------------------------------------------------------------
SummaryNum(dt, "LVDD")

## Exclude one patient as per data provider's advice
dt[DB == "HELS" & LVDD <= 16, LVDD := NA]

# BUN
# ------------------------------------------------------------------------------
SummaryNum(dt, "BUN")

dt[BUN > 900, BUN := NA] # only one value

# Cholesterol
# ------------------------------------------------------------------------------
SummaryNum(dt, "Cholesterol")

dt[Cholesterol <= 50, Cholesterol := NA]

# CRP
# ------------------------------------------------------------------------------
SummaryNum(dt, "CRP")

dt[Baseline_type == "MI", CRP := NA]

# eGFR
# ------------------------------------------------------------------------------
SummaryNum(dt, "eGFR")

# Haemoglobin
# ------------------------------------------------------------------------------
SummaryNum(dt, "Haemoglobin")

dt[Haemoglobin < 2, Haemoglobin := NA]
dt[Haemoglobin > 110, Haemoglobin := NA]

# HbA1c
# ------------------------------------------------------------------------------
SummaryNum(dt, "HbA1c")

dt[HbA1c < 2.5, HbA1c := NA]

# HDL
# ------------------------------------------------------------------------------
SummaryNum(dt, "HDL")

dt[HDL == 0, HDL := NA]

# IL6
# ------------------------------------------------------------------------------
SummaryNum(dt, "IL6")

# LDL
# ------------------------------------------------------------------------------
SummaryNum(dt, "LDL")

dt[LDL == 0, LDL := NA]

# NTProBNP
# ------------------------------------------------------------------------------
SummaryNum(dt, "NTProBNP")

# Potassium
# ------------------------------------------------------------------------------
SummaryNum(dt, "Potassium")

# Sodium
# ------------------------------------------------------------------------------
SummaryNum(dt, "Sodium")

dt[Sodium < 99, Sodium := NA]

# Triglycerides
# ------------------------------------------------------------------------------
SummaryNum(dt, "Triglycerides")

dt[Triglycerides < 20, Triglycerides := NA]

# Troponin_T
# ------------------------------------------------------------------------------
SummaryNum(dt, "Troponin_T")

dt[Baseline_type == "MI", Troponin_T := NA]

# TSH
# ------------------------------------------------------------------------------
SummaryNum(dt, "TSH")

dt[TSH == 0, TSH := NA]

# HR
# ------------------------------------------------------------------------------
SummaryNum(dt, "HR")

dt[Baseline_type == "MI", HR := NA]

dt[HR < 25, HR := NA]
dt[HR > 140, HR := NA]

# PR
# ------------------------------------------------------------------------------
SummaryNum(dt, "PR")

dt[PR <= 50, PR := NA]
dt[PR > 1000, PR := NA]

# QRS
# ------------------------------------------------------------------------------
SummaryNum(dt, "QRS")

dt[QRS < 50, QRS := NA]

# QTc
# ------------------------------------------------------------------------------
SummaryNum(dt, "QTc")

dt[QTc <= 250, QTc := NA]

GetUpperLim(dt, "QTc")

dt[QTc > 790, .N]

dt[QTc > 790, QTc := NA]

# AV_block
# ------------------------------------------------------------------------------
SummaryCat(dt, "AV_block")

dt[Baseline_type == "MI", AV_block := NA]

# AV_block_II_or_III
# ------------------------------------------------------------------------------
SummaryCat(dt, "AV_block_II_or_III")

dt[Baseline_type == "MI", AV_block_II_or_III := NA]

# LBBB
# ------------------------------------------------------------------------------
SummaryCat(dt, "LBBB")

# RBBB
# ------------------------------------------------------------------------------
SummaryCat(dt, "RBBB")

# HR_average
# ------------------------------------------------------------------------------
dt[, HR_average := NULL]

# SDNN
# ------------------------------------------------------------------------------
dt[, SDNN := NULL]

# MI_history
# ------------------------------------------------------------------------------
SummaryCat(dt, "MI_history")

# Time_1st_MI
# ------------------------------------------------------------------------------
dt[, Time_1st_MI := NULL]

# Time_index_MI
# ------------------------------------------------------------------------------
SummaryNum(dt, "Time_index_MI")

dt[is.na(Time_index_MI), Time_index_MI := 40 / 30.44]

setnames(dt, "Time_index_MI", "Time_index_MI_CHD")

# HF
# ------------------------------------------------------------------------------
SummaryCat(dt, "HF")

# Stroke_TIA
# ------------------------------------------------------------------------------
SummaryCat(dt, "Stroke_TIA")

# NSVT
# ------------------------------------------------------------------------------
SummaryCat(dt, "NSVT")

# AF_atrial_flutter: AF, Atrial_flutter
# ------------------------------------------------------------------------------
SummaryCat(dt, "AF_atrial_flutter")

## In PROSE, the AF = AF_atrial_flutter, so drop AF
dt[DB == "PRSE", AF := NA]

dt[AF == "Yes" | Atrial_flutter == "Yes", AF_atrial_flutter := "Yes"]
dt[AF == "No" & Atrial_flutter == "No", AF_atrial_flutter := "No"]

dt[AF == "No" & is.na(Atrial_flutter), AF_atrial_flutter := NA]
dt[Atrial_flutter == "No" & is.na(AF), AF_atrial_flutter := NA]

dt[, .N, keyby = .(AF, Atrial_flutter, AF_atrial_flutter)]

dt[, AF := NULL]
dt[, Atrial_flutter := NULL]

# Anaemia
# ------------------------------------------------------------------------------
dt[, Anaemia := NULL]

# Cancer
# ------------------------------------------------------------------------------
SummaryCat(dt, "Cancer")

# Pulmonary_disease
# ------------------------------------------------------------------------------
dt[, Pulmonary_disease := NULL]

# COPD
# ------------------------------------------------------------------------------
## Remove COPD from ISRL, it's not COPD but chronic lung disease
dt[DB == "ISRL", COPD := NA]

SummaryCat(dt, "COPD")

# Pulmonary_embolism
# ------------------------------------------------------------------------------
dt[, Pulmonary_embolism := NULL]

# Dementia
# ------------------------------------------------------------------------------
SummaryCat(dt, "Dementia")

# Diabetes
# ------------------------------------------------------------------------------
SummaryCat(dt, "Diabetes")

# Hyperlipidaemia
# ------------------------------------------------------------------------------
SummaryCat(dt, "Hyperlipidaemia")

dt[, Hyperlipidaemia := NULL]

# Hypertension
# ------------------------------------------------------------------------------
SummaryCat(dt, "Hypertension")

# Liver_disease
# ------------------------------------------------------------------------------
dt[, Liver_disease := NULL]

# Renal_disease
# ------------------------------------------------------------------------------
SummaryCat(dt, "Renal_disease")

dt[, Renal_disease := NULL]

# VD
# ------------------------------------------------------------------------------
dt[, VD :=  NULL]

# Valve_RR
# ------------------------------------------------------------------------------
dt[, Valve_RR := NULL]

# MR
# ------------------------------------------------------------------------------
SummaryCat(dt, "MR")

dt[, MR := factor(as.character(MR),
                  levels = c("None or mild", "Moderate", "Severe"))]
dt[, .N, keyby = MR]

# FH_CAD
# ------------------------------------------------------------------------------
SummaryCat(dt, "FH_CAD")

# FH_SCD
# ------------------------------------------------------------------------------
SummaryCat(dt, "FH_SCD")

# Alcohol
# ------------------------------------------------------------------------------
SummaryCat(dt, "Alcohol")

# Smoking
# ------------------------------------------------------------------------------
SummaryCat(dt, "Smoking")

# MRI_LVEF
# ------------------------------------------------------------------------------
SummaryNum(dt, "MRI_LVEF")

# Infarct_size
# ------------------------------------------------------------------------------
SummaryNum(dt, "Infarct_size")

# Greyzone_size
# ------------------------------------------------------------------------------
SummaryNum(dt, "Greyzone_size")

# Total_scar
# ------------------------------------------------------------------------------
SummaryNum(dt, "Total_scar")

# LV_mass
# ------------------------------------------------------------------------------
SummaryNum(dt, "LV_mass")

# LVEDV
# ------------------------------------------------------------------------------
SummaryNum(dt, "LVEDV")

# LVESV
# ------------------------------------------------------------------------------
SummaryNum(dt, "LVESV")

# MRI_time_baseline
# ------------------------------------------------------------------------------
dt[, MRI_time_baseline := NULL]

# Time_zero_Y
# ------------------------------------------------------------------------------
SummaryNum(dt, "Time_zero_Y")

# ACE_inhibitor
# ------------------------------------------------------------------------------
SummaryCat(dt, "ACE_inhibitor")

# ARB
# ------------------------------------------------------------------------------
SummaryCat(dt, "ARB")

# ACE_inhibitor_ARB
# ------------------------------------------------------------------------------
SummaryCat(dt, "ACE_inhibitor_ARB")

dt[, .N, keyby = .(ACE_inhibitor_ARB, ACE_inhibitor, ARB)]

dt[ACE_inhibitor == "Yes" | ARB == "Yes", ACE_inhibitor_ARB := "Yes"]
dt[ACE_inhibitor == "No" & ARB == "No", ACE_inhibitor_ARB := "No"]

dt[ACE_inhibitor_ARB == "No" & is.na(ACE_inhibitor), ACE_inhibitor := "No"]
dt[ACE_inhibitor_ARB == "No" & is.na(ARB), ARB := "No"]

dt[, .N, keyby = .(ACE_inhibitor_ARB, ACE_inhibitor, ARB)]

# Aldosterone_antagonist
# ------------------------------------------------------------------------------
SummaryCat(dt, "Aldosterone_antagonist")

# Anti_anginal
# ------------------------------------------------------------------------------
SummaryCat(dt, "Anti_anginal")

# Anti_arrhythmic: Anti_arrhythmic_III, Anti_arrhythmic_1C
# ------------------------------------------------------------------------------
SummaryCat(dt, "Anti_arrhythmic")

dt[, .N, keyby = .(Anti_arrhythmic, Anti_arrhythmic_III, Anti_arrhythmic_1C)]

dt[Anti_arrhythmic == "No" & is.na(Anti_arrhythmic_III),
   Anti_arrhythmic_III := "No"]

dt[, Anti_arrhythmic_1C := NULL]
dt[, Anti_arrhythmic := NULL]

# Anti_coagulant: Anti_coagulant_nonVKA, Anti_coagulant_VKA
# ------------------------------------------------------------------------------
SummaryCat(dt, "Anti_coagulant")

dt[, .N, keyby = .(Anti_coagulant, Anti_coagulant_VKA, Anti_coagulant_nonVKA)]

dt[Anti_coagulant_VKA == "Yes" | Anti_coagulant_nonVKA == "Yes",
   Anti_coagulant := "Yes"]

dt[Anti_coagulant_VKA == "No" & Anti_coagulant_nonVKA == "No",
   Anti_coagulant := "No"]

dt[Anti_coagulant_VKA == "No" & is.na(Anti_coagulant_nonVKA),
   Anti_coagulant := "No"]

dt[, .N, keyby = .(Anti_coagulant, Anti_coagulant_VKA, Anti_coagulant_nonVKA)]

dt[, Anti_coagulant_nonVKA := NULL]
dt[, Anti_coagulant_VKA := NULL]

# Anti_diabetic
# ------------------------------------------------------------------------------
SummaryCat(dt, "Anti_diabetic")

# Anti_diabetic_insulin
# ------------------------------------------------------------------------------
SummaryCat(dt, "Anti_diabetic_insulin")

# Anti_diabetic_oral
# ------------------------------------------------------------------------------
SummaryCat(dt, "Anti_diabetic_oral")

# Diabetes and anti-diabetic medication
# ------------------------------------------------------------------------------
dt[, .N, keyby = .(Diabetes, Anti_diabetic, Anti_diabetic_insulin,
                   Anti_diabetic_oral)]

dt[, .N, keyby = .(Diabetes, Anti_diabetic, Anti_diabetic_insulin,
                   Anti_diabetic_oral, DB)]

dt[Diabetes == "No", Anti_diabetic := "No"]
dt[Diabetes == "No", Anti_diabetic_insulin := "No"]
dt[Diabetes == "No", Anti_diabetic_oral := "No"]

dt[Anti_diabetic_oral == "Yes" | Anti_diabetic_insulin == "Yes",
   Anti_diabetic := "Yes"]

dt[Anti_diabetic_oral == "No" & Anti_diabetic_insulin == "No",
   Anti_diabetic := "No"]

# Anti_platelet
# ------------------------------------------------------------------------------
SummaryCat(dt, "Anti_platelet")

# Beta_blockers
# ------------------------------------------------------------------------------
SummaryCat(dt, "Beta_blockers")

# Calcium_antagonists
# ------------------------------------------------------------------------------
SummaryCat(dt, "Calcium_antagonists")

# Digitalis_glycosides
# ------------------------------------------------------------------------------
SummaryCat(dt, "Digitalis_glycosides")

# Diuretics
# ------------------------------------------------------------------------------
SummaryCat(dt, "Diuretics")

# Lipid_lowering_statins and Lipid_lowering_other
# ------------------------------------------------------------------------------
dt[, Lipid_lowering := NA_character_]

dt[Lipid_lowering_statins == "Yes" | Lipid_lowering_other == "Yes",
   Lipid_lowering := "Yes"]

dt[Lipid_lowering_statins == "No" & Lipid_lowering_other == "No",
   Lipid_lowering := "No"]

dt[Lipid_lowering_statins == "No" & is.na(Lipid_lowering_other),
   Lipid_lowering := "No"]

dt[, Lipid_lowering := as.factor(Lipid_lowering)]

dt[, .N, keyby = .(Lipid_lowering_statins, Lipid_lowering_other, Lipid_lowering)]

dt[, Lipid_lowering_other := NULL]
dt[, Lipid_lowering_statins := NULL]

# Boxplots
# ------------------------------------------------------------------------------
cdm <- fread("../etc/profid-common-data-model.csv")
dim(cdm)

## Boxplots of numeric variables
boxplots <- list()

cols.num <- unlist(lapply(dt, is.numeric))
cols.num <- names(cols.num)[cols.num == TRUE]
cols.num <- cols.num[!(cols.num %in% c("DB", "ID", "Status", "Survival_time"))]
stopifnot(all(cols.num %in% cdm[, `Variable name`]))

cols.cdm <- cdm[`Variable name` %in% cols.num, `Variable name`]
cols.num <- cols.num[match(cols.cdm, cols.num)]

for (j in cols.num) {
  boxplots[[j]] <-
    ggplot(dt, aes_string(x = "DB", y = j)) +
    geom_boxplot() +
    facet_wrap( ~ DB, scales = "free_x", nrow = 1) +
    theme_bw() +
    theme(text = element_text(size = 8),
          axis.text.x = element_blank(),
          axis.title.x = element_blank()) +
    ggtitle(j)
}

## multi.page <- ggarrange(plotlist = boxplots, nrow = 1, ncol = 1)
## ggexport(multi.page, filename = "../../z-results/misc/numeric-vars-boxplots.pdf")

## Clean up
rm(j, cdm, cols.cdm, cols.num, boxplots, multi.page)

# Additional non-CDM variables
# ==============================================================================
# Endpoint type
# ------------------------------------------------------------------------------
dt[, .N, keyby = Endpoint_type]
dt[, .N, .(DB, Endpoint_type)]

# Baseline type
# ------------------------------------------------------------------------------
dt[, .N, keyby = Baseline_type]
dt[, .N, keyby = .(DB, Baseline_type)]

# SWHR indicator
# ------------------------------------------------------------------------------
dt[, IsSWHR := 0]
dt[DB == "SWHR", IsSWHR := 1]
dt[, .N, IsSWHR]

# CVD risk regions
# ------------------------------------------------------------------------------
(cvd.regions <- fread("../etc/region-risk-degrees.csv"))
setnames(cvd.regions, "Region", "CVD_risk_region")

pat.country <- fread("../etc/patient-centre-country.csv")
dim(pat.country)

stopifnot(all(pat.country$Country %in% cvd.regions$Country))

pat.country <- merge(pat.country, cvd.regions, by = "Country", sort = FALSE)
stopifnot(all(dt$ID %in% pat.country$ID))

pat.country[DB %in% c("PRSI", "PRSL"), DB := "PRSE"]

dt <- merge(dt, pat.country[, .(DB, ID, CVD_risk_region)],
            by = c("DB", "ID"), all.x = TRUE, sort = FALSE)
stopifnot(!anyNA(dt$CVD_risk_region))

dt[, CVD_risk_region :=
       factor(CVD_risk_region,
              levels = c("Low", "Moderate", "High", "Very high"),
              ordered = TRUE)]

dt[, CVD_risk_region := as.integer(CVD_risk_region)]

dt[, .N, keyby = .(CVD_risk_region)]

## Clean up
rm(cvd.regions, pat.country)

# Drop variables with all data missing or a single unique value
# ==============================================================================
allna <- unlist(lapply(dt, function(x) all(is.na(x))))
allna[allna]

## dt <- dt[, allna == FALSE, with = FALSE]
## dim(dt)

## Drop variables with only a single unique value (no variation)
v1 <- unlist(lapply(dt, function(x) length(unique(x[!is.na(x)])) == 1))
v1[v1]

## dt <- dt[, v1 == FALSE, with = FALSE]
## dim(dt)

## Clean up
rm(allna, v1)

# Exclude some variables based on availability
# ==============================================================================
# Drop variables that appear only in one dataset
inone <- dt[, lapply(.SD, function(x) !all(is.na(x))), DB]
inone <- colSums(inone[, -c("ID", "DB")])
inone <- inone[inone == 1]
names(inone)

# MRI
# ==============================================================================
## Datasets with MRI data: ASTN, DRVT, NANC, HELS, PRSL, PRDT

## Specify patients with MRI data
dt[, HasMRI := NA_character_]

## ASTN: all have MRI data
dt[DB == "ASTN", HasMRI := "Yes"]

## DRVT: all have MRI data
dt[DB == "DRVT", HasMRI := "Yes"]

## NANC: all have MRI data
dt[DB == "NANC", HasMRI := "Yes"]

## HELS: only patients with non-missing MRI_LVEF have MRI data
dt[DB == "HELS" & !is.na(MRI_LVEF), HasMRI := "Yes"]
## Note: this has been modified below

## PRSE: PRSL patients have MRI data
dt[DB == "PRSE" & grepl("^PRSL_", ID), .N,
   .(lvef_na = is.na(MRI_LVEF),
     inf_na = is.na(Infarct_size),
     gz.na = is.na(Greyzone_size))]

dt[DB == "PRSE" & grepl("^PRSL_", ID), HasMRI := "Yes"]

## PRDT: check missing values in primary MRI variables
dt[DB == "PRDT", .N, .(lvef_na = is.na(MRI_LVEF),
                       inf_na = is.na(Infarct_size),
                       gz.na = is.na(Greyzone_size))]

dt[DB == "PRDT" &
     !is.na(MRI_LVEF) | !is.na(Infarct_size) | !is.na(Greyzone_size),
   HasMRI := "Yes"]
## This is a small bug with the following consequences: all patients with
## non-missing values in any of the Infarct_size or Greyzone_size will be marked
## as MRI patients across all MRI datasets. The intention was:
##
## dt[DB == "PRDT" &
##      (!is.na(MRI_LVEF) | !is.na(Infarct_size) | !is.na(Greyzone_size)),
##    HasMRI := "Yes"]
##
## However, this doesn't have any real consequences for the analysis. It
## modifies the selection of patients only for HELS, where a patient will be
## marked as MRI patient if they additionally have non-missing data in
## Infarct_size or Greyzone_size; this adds 22 patients with MRI data to HELS.
## Their MRI LVEF data was provided by the data provider upon request.

dt[is.na(HasMRI), HasMRI := "No"]

dt[, .N, keyby = .(DB, HasMRI)]

## Add _MRI suffix to patients with MRI data
stopifnot(dt[grepl("_MRI$", ID), .N] == 0)
dt[HasMRI == "Yes", ID := paste0(ID, "_MRI")]

# Output
# ==============================================================================
dim(dt)

saveRDS(dt, "../output/datastack.rds")
