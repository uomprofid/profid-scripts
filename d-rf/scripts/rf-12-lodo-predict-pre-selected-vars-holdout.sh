#!/bin/bash --login
#$ -cwd
#$ -o /dev/null
#$ -e /dev/null
#$ -pe smp.pe 12
#$ -t 1-18

export OMP_NUM_THREADS=${NSLOTS}

module load apps/gcc/R/3.6.1

R_FILE=./rf-12-lodo-predict-pre-selected-vars-holdout.R

LOG_FILE=../logs/rf-12-lodo-predict-pre-selected-vars-holdout-${SGE_TASK_ID}.Rout
[ -f ${LOG_FILE} ] && rm ${LOG_FILE}

R CMD BATCH --vanilla ${R_FILE} ${LOG_FILE}
