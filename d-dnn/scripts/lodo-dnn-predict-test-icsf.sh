#!/bin/bash --login

export OMP_NUM_THREADS=4

# Cap on number of variables
# ---------------------------
if [ -z "$1" ]; then
	CAP=-1
	CAP_STR="nocap"
else
    # Possible options: cap10, cap20, nocap-var-sel
	CAP_STR="$1"
fi

for SGE_TASK_ID in {1..18}; do

	exec 1> ../logs/${CAP_STR}-lodo-dnn-predict-test-${SGE_TASK_ID}.log 2>&1

	echo -e "Doing ${SGE_TASK_ID} ...\n"

	# Input files
	# ------------
	DT_DIR=../../c-data/data

	DB=$(sed -n "${SGE_TASK_ID}"p ${DT_DIR}/db-names-lodo.csv)

	DT_PATH=${DT_DIR}/lodo/$DB"-test-imputed-inter.csv"

	if [ ! -f ${DT_PATH} ]; then
		echo "File ${DT_PATH} not found."
		exit -1
	fi

	echo -e "Test data file:\n ${DT_PATH}"

	echo $(md5sum ${DT_PATH})

	# Directory for model files
	# --------------------------------
	MOD_DIR=../models/${CAP_STR}/lodo/${DB}

	if [ ! -d ${MOD_DIR} ]; then
		echo "Model directory not found."
		exit -1
	fi

	echo -e "\nModel directory:\n ${MOD_DIR}\n"

	# Fixed time predictions files
	# ----------------------------
	FT_TIMES_PATH=${DT_DIR}/lodo/${DB}-prediction-times-fixed.csv
	FT_OUT_PATH=../predictions/${CAP_STR}/lodo/${DB}-test-predictions-fixed.csv

	if [ ! -f ${FT_TIMES_PATH} ]; then
		echo "File ${FT_TIMES_PATH} not found."
		exit -1
	fi

	# Do predictions
	# --------------------
	echo -e "Fixed time predictions: \n${FT_TIMES_PATH} \n${FT_OUT_PATH}\n"

	~/rds/profid/spectra/venv-icsf/bin/dnn_predict \
        -d ${DT_PATH} \
        -m ${MOD_DIR} \
		-o ${FT_OUT_PATH} \
		-t ${FT_TIMES_PATH};

	# Predictions for IBS
	# ----------------------
	IBS_TIMES_PATH=${DT_DIR}/lodo/${DB}-test-prediction-times-ibs-grid.csv
	IBS_OUT_PATH=../predictions/${CAP_STR}/lodo/${DB}-test-predictions-ibs-grid.csv

	if [ ! -f ${IBS_TIMES_PATH} ]; then
		echo "File ${PRED_TIMES_PATH} not found."
		exit -1
	fi

	echo -e "\n\nIBS grid predictions \n${IBS_TIMES_PATH} \n${IBS_OUT_PATH}\n"

	~/rds/profid/spectra/venv-icsf/bin/dnn_predict \
        -d ${DT_PATH} \
		-m ${MOD_DIR} \
		-o ${IBS_OUT_PATH} \
		-t ${IBS_TIMES_PATH};

	echo "\n\nDone."

done;
