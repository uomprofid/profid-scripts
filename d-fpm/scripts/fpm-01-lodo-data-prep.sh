#!/bin/bash --login
#$ -cwd
#$ -o /dev/null
#$ -e /dev/null
#$ -pe smp.pe 8
#$ -t 1-18

export OMP_NUM_THREADS=${NSLOTS}

module load apps/gcc/R/3.6.1

R_FILE=./fpm-01-lodo-data-prep.R

LOG_FILE=../logs/${R_FILE}.out.${SGE_TASK_ID}
[ -f ${LOG_FILE} ] && rm ${LOG_FILE}

R CMD BATCH --vanilla ${R_FILE} ${LOG_FILE}
