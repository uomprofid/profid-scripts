# Description
# ==============================================================================
## Prepare data for FPM. Use pre-selected variables only.

rm(list = ls())

Sys.setenv(LANG = "en")
Sys.setlocale("LC_ALL", "English")

# Requirements
# ==============================================================================
library(data.table)
library(tools)
library(survival)
library(rms)

source("./hz-splines.R")

# Input/output files and directories
# ==============================================================================
## Output directory
out.dir.path <- "../data/processed/fpm/uom/"
if (!dir.exists(out.dir.path)) dir.create(out.dir.path)

uom.vars <- fread("../input/misc/fpm-devstack-column-names.csv")
dim(uom.vars)

# Training data
# ============================================================================
(train.path <- "../data/processed/uom-train-imputed-inter-pre-selected-vars.csv")
stopifnot(file.exists(train.path))

print(md5sum(train.path))

train <- fread(train.path)
(dim(train))

# Natural splines
# ============================================================================
## Variables to model with natural splines
spline.vars <- c("Age", "LVEF", "eGFR_log1p")

ns.list <- HZ.natural_splines_train(train, spline.vars, 4)

train <- HZ.natural_splines_add(train, ns.list)

# Specify variables for each competing risk
# ============================================================================
e1.vars <- uom.vars[grepl("^e1_", Variable), Variable]
e2.vars <- uom.vars[grepl("^e2_", Variable), Variable]

e1.vars <- gsub("^e1_", "", e1.vars)
e2.vars <- gsub("^e2_", "", e2.vars)

stopifnot(all(e1.vars %in% colnames(train)))
stopifnot(all(e2.vars %in% colnames(train)))

# Training data
# ----------------------------------------------------------------------------
e1.train <- train[, c("ID", "Status", "Survival_time", e1.vars), with = FALSE]
setnames(e1.train, e1.vars, paste0("e1_", gsub(" +", "_", e1.vars)))

e2.train <- train[, c("ID", e2.vars), with = FALSE]
setnames(e2.train, e2.vars, paste0("e2_", gsub(" +", "_", e2.vars)))

train.fpm <- merge(e1.train, e2.train, by = "ID")

fwrite(train.fpm, sprintf("%s/uom-fpm-train.csv", out.dir.path))

# Prediction times
# ============================================================================
pt.fixed <- fread("../input/misc/devstack-prediction-times-fixed.csv")
pt.ibs <- fread("../input/misc/devstack-prediction-times-ibs-grid.csv")

# WDHR train data
# ----------------------------------------------------------------------------
train.fpm[, Status := NA]
train.fpm[, Survival_time := NULL]

pt.fixed.train <- as.data.table(expand.grid(ID = train.fpm$ID, Time = pt.fixed$Time))
pt.fixed.train[, Time_ID := .GRP, by = Time]

train.fpm.fixed <- merge(pt.fixed.train, train.fpm, by = "ID", sort = FALSE)
dim(train.fpm.fixed)

fwrite(train.fpm.fixed, sprintf("%s/uom-fpm-train-fixed.csv", out.dir.path))

pt.ibs.train <- as.data.table(expand.grid(ID = train.fpm$ID, Time = pt.ibs$Time))
pt.ibs.train[, Time_ID := .GRP, by = Time]

train.fpm.ibs <- merge(pt.ibs.train, train.fpm, by = "ID", sort = FALSE)
dim(train.fpm.ibs)

fwrite(train.fpm.ibs, sprintf("%s/uom-fpm-train-ibs.csv", out.dir.path))
