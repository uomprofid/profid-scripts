#!/bin/bash --login
#$ -cwd
#$ -o /dev/null
#$ -e /dev/null
#$ -pe smp.pe 4
#$ -t 1

export OMP_NUM_THREADS=${NSLOTS}

module load apps/gcc/R/3.6.1

R_FILE=./impute-2-devstack.R

LOG_FILE=../logs/impute-2-devstack.Rout
[ -f ${LOG_FILE} ] && rm ${LOG_FILE}

R CMD BATCH --vanilla ${R_FILE} ${LOG_FILE}
