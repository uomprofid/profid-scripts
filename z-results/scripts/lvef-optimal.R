# Description
# ==============================================================================
## Estimate optimal LVEF cut-off value based on Youden's index.

rm(list = ls())

# Requirements
# ==============================================================================
library(data.table)
library(ggplot2)

source("./utils/hz-prediction-performance-measures.R")

# Data
# ==============================================================================
## Outcome data for imputed UOM development datastack
dt <- fread("../../c-data/data/devstack-imputed-inter.csv")
dt <- dt[, .(ID, Status, Survival_time, LVEF)]

dt[, DB := gsub("([A-Z]{4})_.+", "\\1", ID)]
head(dt)
dim(dt)

dt[, Status_SCD := as.integer(Status == 1)]

# Optimal LVEF cut-off global
# ==============================================================================
Youden_index <- function(thresh, y, lvef) {
  HZ.cpm(y, lvef, thresh)$youden
}

(lvef.grid <- seq(25, 50))

youden <- sapply(lvef.grid, Youden_index, y = dt$Status_SCD, lvef = dt$LVEF)

(lvef.opt <- lvef.grid[which.max(youden)])


cpm.35 <- HZ.cpm(dt$Status_SCD, dt$LVEF, 35)
cpm.35 <- as.data.table(cpm.35)

cpm.opt <- HZ.cpm(dt$Status_SCD, dt$LVEF, lvef.opt)
cpm.opt <- as.data.table(cpm.opt)

res1 <- rbind(cpm.35, cpm.opt)
setnames(res1, "threshold", "LVEF_threshold")

fwrite(res1, "../misc/lvef-optimal-devstack.csv")

## Plot
b1 <- data.table(LVEF = lvef.grid, youden = youden)

p1 <- ggplot(b1, aes(x = LVEF, y = youden)) +
  geom_point(size = 2) +
  xlab("LVEF (%)") + ylab("Youden's index") +
  theme_bw() +
  theme(text = element_text(size = 12, colour = "black"),
      axis.text = element_text(colour = "black"))
p1

ggsave("../misc/lvef-optimal-devstack.tiff", p1,
       width = 27, height = 21, units = "cm", dpi = 300)

## Net re-classification error
dt.nri <- dt[, .(Status, LVEF)]

dt.nri[Status == 2, Status := 0]

dt.nri[, Status_LVEF_35 := ifelse(LVEF <= 35, 1, 0)]
dt.nri[, Status_LVEF_opt := ifelse(LVEF <= lvef.opt, 1, 0)]

dt.nri[, NRI_score := NA_integer_]
dt.nri[Status_LVEF_35 == 0 & Status_LVEF_opt == 0, NRI_score := 0]
dt.nri[Status_LVEF_35 == 1 & Status_LVEF_opt == 1, NRI_score := 0]

dt.nri[Status == 0 & Status_LVEF_35 == 1 & Status_LVEF_opt == 0, NRI_score := 1]
dt.nri[Status == 1 & Status_LVEF_35 == 0 & Status_LVEF_opt == 1, NRI_score := 1]

dt.nri[Status == 0 & Status_LVEF_35 == 0 & Status_LVEF_opt == 1, NRI_score := -1]
dt.nri[Status == 1 & Status_LVEF_35 == 1 & Status_LVEF_opt == 0, NRI_score := -1]

(nri <- dt.nri[, .(nri_status = sum(NRI_score) / .N), Status])

## Net re-classification error
sum(nri$nri_status)
lvef.opt

# Optimal LVEF cut-off values in LODO
# ==============================================================================
res.lodo <- list()

for(db in unique(dt$DB)) {

  print(db)

  train <- dt[DB != db]
  test <- dt[DB == db]

  db.ydn <- sapply(lvef.grid, Youden_index, y = train$Status_SCD, lvef = train$LVEF)
  db.lvef.opt <- lvef.grid[which.max(db.ydn)]

  db.cpm <- HZ.cpm(test$Status_SCD, test$LVEF, db.lvef.opt)

  res.lodo[[db]] <- as.data.table(db.cpm)[, DB := db]
}

(res.lodo <- rbindlist(res.lodo))

fwrite(res.lodo, "../misc/lvef-optimal-lodo.csv")

res.lodo[, DB_lab := sprintf("%s\n(%d)", DB, threshold)]
res.lodo <- res.lodo[order(youden)]
res.lodo[, DB_lab := factor(DB_lab, levels = DB_lab)]

p2 <- ggplot(res.lodo, aes(x = DB_lab, y = youden)) +
  geom_point(size = 2) +
  xlab("Test dataset\n (Optimal LVEF %)") + ylab("Youden's index") +
  theme_bw() +
  theme(text = element_text(size = 12, colour = "black"),
      axis.text = element_text(colour = "black"))
p2

ggsave("../misc/lvef-optimal-lodo.tiff", p2,
       width = 27, height = 21, units = "cm", dpi = 300)
