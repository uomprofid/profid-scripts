clear all

// Set number of cores
set processors 4

// Command line arguments
args data_file model_file  output_file
display "`data_file'"
display "`model_file'"
display "`output_file'"

// Read the data file
insheet using "`data_file'"

// Set the survival time variables
estimates use "`model_file'"

generate _status = .

capture predict xb, xb timevar(time)

outsheet id time xb_c1 xb_c2 ///
    using "`output_file'", comma replace
