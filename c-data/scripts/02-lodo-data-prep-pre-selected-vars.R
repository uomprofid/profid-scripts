# Description
# ==============================================================================
## Prepare data for leave-one-dataset-out analysis. Add interactions and add
## back CVD risk regions variables. Use only pre-selected variables.

rm(list = ls())

options(width = 112, length = 99999)

# Requirements
# ==============================================================================
library(data.table)
library(splines)
library(tools)

source("./hz-interactions-and-splines.R")

# Pre-selected variables
# ==============================================================================
selected.vars <- c(
  "Age",
  "LVEF",
  "Sex_BIN_Male",
  "ACE_inhibitor_ARB_BIN_Yes",
  "Anti_diabetic_insulin_BIN_Yes",
  "Beta_blockers_BIN_Yes",
  "Diabetes_BIN_Yes",
  "AF_atrial_flutter_BIN_Yes",
  "Diuretics_BIN_Yes",
  "Anti_platelet_BIN_Yes",
  "eGFR_log1p",
  "BMI_log1p",
  "Hypertension_BIN_Yes",
  "Lipid_lowering_BIN_Yes",
  "Smoking_BIN_Yes",
  "Anti_coagulant_BIN_Yes",
  "CABG_BIN_Yes",
  "Digitalis_glycosides_BIN_Yes",
  "LBBB_BIN_Yes",
  "Stroke_TIA_BIN_Yes",
  "Time_zero_Y",
  "Endpoint_type_BIN_FAT",
  "Endpoint_type_BIN_FAS",
  "Baseline_type_BIN_MI40d",
  "IsSWHR"
)
## Add CVD risk regions later

# Interactions
# ==============================================================================
interactions <- as.formula(~ Age:LVEF +
                             Age:eGFR_log1p +
                             Baseline_type_BIN_MI40d:LVEF +
                             Baseline_type_BIN_MI40d:eGFR_log1p)

# Input/Output directories
# ==============================================================================
in.dir <- "../../b-imputation/data/lodo"
out.dir <- "../data/lodo"

# Data
# ==============================================================================
dt <- readRDS("../../a-datastack/output/datastack-num.rds")
dim(dt)

db.names <- sort(tolower(unique(dt$DB)))

fwrite(data.table(DB = db.names),
       "../../c-data/data/db-names-lodo.csv", col.names = FALSE)

cvdrr.vars <- c("CVDRR_BIN_geq_2", "CVDRR_BIN_geq_3", "CVDRR_BIN_eq_4")

stopifnot(!anyNA(dt[, ..cvdrr.vars]))

# Data preparation
# ==============================================================================

for (db in db.names) {

  cat("\n\nProcessing:", toupper(db), "\n")
  cat(rep("-", 16), "\n", sep = "")

  if (db == "swhr") {
    selected.vars <- selected.vars[selected.vars != "IsSWHR"]
  }

  # Imputed data
  # ---------------------------------------------------------------------------
  ## Training data
  train.path.imp <- sprintf("%s/%s-train-imputed.csv", in.dir, db)
  train.imp <- fread(train.path.imp)
  setcolorder(train.imp, c("ID", "Status", "Survival_time"))
  stopifnot(!anyNA(train.imp))
  stopifnot(all(train.imp$ID %in% dt$ID))
  cat("\nTrain dim:", dim(train.imp), "\n")
  print(md5sum(train.path.imp))

  stopifnot(all(selected.vars %in% names(train.imp)))
  train.imp <- train.imp[, c("ID", "Status", "Survival_time", selected.vars), with = FALSE]
  print(dim(train.imp))

  ## Test data
  test.path.imp <- sprintf("%s/%s-test-imputed.csv", in.dir, db)
  test.imp <- fread(test.path.imp)
  stopifnot(!anyNA(test.imp))
  stopifnot(all(test.imp$ID %in% dt$ID))
  setcolorder(test.imp, c("ID", "Status", "Survival_time"))
  cat("\nTest dim:", dim(test.imp), "\n")
  print(md5sum(test.path.imp))

  stopifnot(all(selected.vars %in% names(test.imp)))
  test.imp <- test.imp[, c("ID", "Status", "Survival_time", selected.vars), with = FALSE]
  print(dim(test.imp))

  ## Holdout data
  hld.path.imp <- sprintf("%s/%s-holdout-imputed.csv", in.dir, db)
  hld.imp <- fread(hld.path.imp)
  stopifnot(!anyNA(hld.imp))
  stopifnot(all(hld.imp$ID %in% dt$ID))
  setcolorder(hld.imp, c("ID", "Status", "Survival_time"))
  cat("\nHoldout dim:", dim(hld.imp), "\n")
  print(md5sum(hld.path.imp))

  stopifnot(all(selected.vars %in% names(hld.imp)))
  hld.imp <- hld.imp[, c("ID", "Status", "Survival_time", selected.vars), with = FALSE]
  print(dim(hld.imp))

  # CVD regions
  # ---------------------------------------------------------------------------
  train.imp <- merge(train.imp, dt[, c("ID", cvdrr.vars), with = FALSE],
                     by = "ID", all.x = TRUE, sort = FALSE)

  test.imp <- merge(test.imp, dt[, c("ID", cvdrr.vars), with = FALSE],
                    by = "ID", all.x = TRUE, sort = FALSE)

  hld.imp <- merge(hld.imp, dt[, c("ID", cvdrr.vars), with = FALSE],
                   by = "ID", all.x = TRUE, sort = FALSE)

  # Interactions
  # ---------------------------------------------------------------------------
  ## Training data
  train.imp.inter <- HZ.interactions_add(train.imp, interactions)
  dim(train.imp.inter)

  train.imp.inter.path <-
    sprintf("%s/%s-train-imputed-inter-pre-selected-vars.csv", out.dir, db)
  fwrite(train.imp.inter, train.imp.inter.path)

  ## Test data
  test.imp.inter <- HZ.interactions_add(test.imp, interactions)
  dim(test.imp.inter)

  test.imp.inter.path <-
    sprintf("%s/%s-test-imputed-inter-pre-selected-vars.csv", out.dir, db)
  fwrite(test.imp.inter, test.imp.inter.path)

  ## Holdout data
  hld.imp.inter <- HZ.interactions_add(hld.imp, interactions)
  dim(hld.imp.inter)

  hld.imp.inter.path <-
    sprintf("%s/%s-holdout-imputed-inter-pre-selected-vars.csv", out.dir, db)
  fwrite(hld.imp.inter, hld.imp.inter.path)

  # Round imputed binary values for random forests
  # ---------------------------------------------------------------------------
  ## Use only the dataset with interactions
  train.imp.inter.rf <- copy(train.imp.inter)
  test.imp.inter.rf <- copy(test.imp.inter)
  hld.imp.inter.rf <- copy(hld.imp.inter)

  for (x in colnames(train.imp.inter.rf)) {
    if (grepl("_BIN_", x)) {
      stopifnot(x %in% colnames(test.imp.inter.rf))
      stopifnot(all(dt[[x]] %in% c(NA, 0, 1)))
      ## Training data
      train.imp.inter.rf[get(x) < 0, (x) := 0]
      train.imp.inter.rf[get(x) > 1, (x) := 1]
      ## Note: exact 0.5 will be rounded to 0
      train.imp.inter.rf[get(x) >= 0 & get(x) <= 1, (x) := round(get(x))]
      ## Test data
      test.imp.inter.rf[get(x) < 0, (x) := 0]
      test.imp.inter.rf[get(x) > 1, (x) := 1]
      ## Note: exact 0.5 will be rounded to 0
      test.imp.inter.rf[get(x) >= 0 & get(x) <= 1, (x) := round(get(x))]
      ## Holdout data
      hld.imp.inter.rf[get(x) < 0, (x) := 0]
      hld.imp.inter.rf[get(x) > 1, (x) := 1]
      ## Note: exact 0.5 will be rounded to 0
      hld.imp.inter.rf[get(x) >= 0 & get(x) <= 1, (x) := round(get(x))]
    }
  }

  train.imp.inter.rf.path <-
    sprintf("%s/%s-train-imputed-inter-rf-pre-selected-vars.csv", out.dir, db)
  fwrite(train.imp.inter.rf, train.imp.inter.rf.path)

  test.imp.inter.rf.path <-
    sprintf("%s/%s-test-imputed-inter-rf-pre-selected-vars.csv", out.dir, db)
  fwrite(test.imp.inter.rf, test.imp.inter.rf.path)

  hld.imp.inter.rf.path <-
    sprintf("%s/%s-holdout-imputed-inter-rf-pre-selected-vars.csv", out.dir, db)
  fwrite(hld.imp.inter.rf, hld.imp.inter.rf.path)
}
