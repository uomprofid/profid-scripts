##' Fit natural splines with 3 internal knots (DF = 4)
##'
##' @title Fit natural splines with 3 internal knots
##' @param dt data.table
##' @param x.vars character array of numeric variables
##' @param df degrees of freedom for splines
##' @return a list of `ns` objects
##' @author UNIMAN
HZ.natural_splines_train <- function(dt, x.vars, df) {
  require(splines)

  stopifnot(is.data.table(dt))
  stopifnot(!anyNA(dt))

  ns.list <- list()
  for (x in x.vars) {
    if (!(x %in% colnames(dt))) {
      warning(sprintf("%s is not found in the dataset.", x))
      next
    }
    ns.list[[x]] <- ns(dt[[x]], df = df)
  }
  ns.list
}

##' Add natural spline variables to a dataset
##'
##' @title Add natural spline variables
##' @param dt data.table
##' @param ns.list list of `ns` objects, output of HZ.natrual_splines_train
##' @return dt with added natural spline variables
##' @author UNIMAN
HZ.natural_splines_add <- function(dt, ns.list) {
  require(splines)

  stopifnot(is.data.table(dt))
  stopifnot(!anyNA(dt))

  stopifnot(is.list(ns.list))
  stopifnot(all(unlist(lapply(ns.list, is, c("ns", "basis", "matrix")))))
  stopifnot(all(names(ns.list) %in% colnames(dt)))

  dtc <- copy(dt)
  for (x in names(ns.list)) {
    xns <- dtc[[x]]
    names(xns) <- dtc$ID
    mat <- predict(ns.list[[x]], xns, keep.rownames = TRUE)
    colnames(mat) <- paste0(x, "_NS", colnames(mat))
    dtc.ns <- data.table(ID = rownames(mat), mat)
    stopifnot(identical(dtc.ns$ID, dtc$ID))
    dtc <- merge(dtc, dtc.ns, by = "ID", sort = FALSE)
    dtc[, (x) := NULL]
  }
  dtc
}
