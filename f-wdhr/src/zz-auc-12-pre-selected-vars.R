rm(list = ls())

Sys.setenv(LANG = "en")
Sys.setlocale("LC_ALL", "English")

library(data.table)
library(survival)
library(pROC)

## Outcome data
dt <- readRDS("../data/processed/wdhr-num-train.rds")
dt <- dt[, .(DB, ID, Status, Survival_time, LVEF)]
dim(dt)

## LVEF
lvef <- dt[, .(DB = "WDHR", Method = "LVEF", Event = 1, ID, Time = 12, CIF = (100 - LVEF) / 100)]

## Weibull
pred.wbl <- fread("../data/predictions/fpm/uom/weibull/train-predictions-shrunk-fixed.csv")
pred.wbl[, Method := "Weibull"]
pred.wbl[, DB := "WDHR"]
stopifnot(uniqueN(pred.wbl) == nrow(pred.wbl))

## FPM DF2
pred.fpm.df2 <- fread("../data/predictions/fpm/uom/fpm-df2/train-predictions-shrunk-fixed.csv")
pred.fpm.df2[, Method := "FPM"]
pred.fpm.df2[, DB := "WDHR"]
stopifnot(uniqueN(pred.fpm.df2) == nrow(pred.fpm.df2))

## DNN
pred.dnn <- fread("../data/predictions/dnn/pre-sel-vars/wdhr-test-predictions-fixed.csv")
pred.dnn[, Method := "DNN"]
pred.dnn[, DB := "WDHR"]
stopifnot(uniqueN(pred.dnn) == nrow(pred.dnn))

pred <- rbind(pred.wbl[Event == 1, .(Method, LODB = DB, ID, Event, Time, CIF)],
              pred.fpm.df2[Event == 1, .(Method, LODB = DB, ID, Event, Time, CIF)],
              pred.dnn[Event == 1, .(Method, LODB = DB, ID, Event, Time, CIF)],
              lvef[Event == 1, .(Method, LODB = DB, ID, Event, Time, CIF)])
dim(pred)

all(pred$ID %in% dt$ID)

pred[, ID := as.character(ID)]

pred <- pred[Time == 12]
dim(pred)

## AUC
dauc <- copy(pred)

dauc <- merge(dauc, dt, by = "ID")

dauc <- dauc[!(Survival_time < 12 & Status == 0)]
dim(dauc)

dauc[, Status_12 := NA_integer_]
dauc[Survival_time > 12, Status_12 := 0]
dauc[Survival_time <= 12 & Status == 1, Status_12 := 1]
dauc[Survival_time <= 12 & Status == 2, Status_12 := 0]

dauc[, .N, keyby = .(Status, Status_12)]

a1 <- dauc[, .(auc = auc(roc(Status_12, CIF))[1],
               var = var(roc(Status_12, CIF))[1]),
           .(Method, LODB)]
a1[, se := sqrt(var)]
a1

## Add N
n <- dt[, .N, .(LODB = DB)]
pdt <- merge(a1, n, by = "LODB", all.x = TRUE)
pdt[, LODB := paste0(LODB, "\n", N)]
pdt <- pdt[order(auc)]
pdt

pdt[, llim := auc - 1.96 * se]
pdt[, ulim := auc + 1.96 * se]
pdt

fwrite(pdt, "../send-to-uom/auc12/wdhr-auc-12-pre-sel-vars.csv")
