#!/bin/bash --login
#$ -cwd
#$ -o /dev/null
#$ -e /dev/null
#$ -pe smp.pe 8
#$ -t 1

export OMP_NUM_THREADS=${NSLOTS}

module load apps/gcc/R/3.6.1

# Cap on number of variables
# ---------------------------
if [ -z "$1" ]; then
    CAP=-1
    CAP_STR="nocap"
else
    CAP="$1"
    CAP_STR="cap"${CAP}
fi

exec 1> ../logs/${CAP_STR}-devstack-dnn-fit-${SGE_TASK_ID}.log 2>&1

# Input files
# ------------
DT_PATH=../../c-data/data/devstack-imputed-inter.csv

if [ ! -f ${DT_PATH} ]; then
 echo "File ${DT_PATH} not found."
 exit -1
fi

echo -e "Training data file:\n ${DT_PATH}\n"

echo $(md5sum ${DT_PATH})

# Output directory for model files
# --------------------------------
MOD_DIR=../models/${CAP_STR}/devstack

if [ -d ${MOD_DIR} ]; then
    rm ${MOD_DIR}/*.*
else
    mkdir -p ${MOD_DIR}
fi

echo -e "\nModel directory:\n ${MOD_DIR}\n"

# Run DNN model fitter
# --------------------
echo -e "Fitting a model with ${CAP_STR} on variables ...\n\n"

~/rds/profid/spectra/venv-csf/bin/dnn_fit -d ${DT_PATH} \
                                          -o ${MOD_DIR} \
                                          -v ${CAP};

echo "\n\nDone."
