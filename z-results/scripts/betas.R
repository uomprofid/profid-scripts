rm(list = ls())

library(data.table)
library(openxlsx)

## Weibull
b1.lf <- list.files("../../d-fpm/models/pre-sel-vars/weibull/lodo/",
                    pattern = "betas", full.names = TRUE)
b1.lf

b1 <- list()
for (f in b1.lf) {
  db <- gsub("([a-z]{4})-betas.xlsx", "\\1", basename(f))
  bf <- as.data.table(read.xlsx(f, colNames = FALSE))
  setnames(bf, c("Cause", "Variable", "Estimate"))
  bf[, DB := toupper(db)]
  setcolorder(bf, "DB")
  b1[[db]] <- bf
}

b1 <- rbindlist(b1)

b1[, HR := exp(Estimate)]

b1 <- b1[!grepl("^_cons", Variable)]
b1 <- b1[!grepl("^_rcs_", Variable)]

t1 <- dcast(b1, Cause + Variable ~ DB, value.var = "HR")

t1[, Cause := factor(Cause, levels = c("SCD", "Other"))]
t1 <- t1[order(Cause, Variable)]
t1[1:5, 1:5]

fwrite(t1, "../lodo/betas-weibull-full.csv")

rm(b1, b1.lf, bf, db, f, t1)

## FPM
b2.lf <- list.files("../../d-fpm/models/pre-sel-vars/fpm-df2/lodo/",
                    pattern = "betas", full.names = TRUE)
b2.lf

b2 <- list()
for (f in b2.lf) {
  db <- gsub("([a-z]{4})-betas.xlsx", "\\1", basename(f))
  bf <- as.data.table(read.xlsx(f, colNames = FALSE))
  setnames(bf, c("Cause", "Variable", "Estimate"))
  bf[, DB := toupper(db)]
  setcolorder(bf, "DB")
  b2[[db]] <- bf
}

b2 <- rbindlist(b2)

b2[, HR := exp(Estimate)]

b2 <- b2[!grepl("^_cons", Variable)]
b2 <- b2[!grepl("^_rcs_", Variable)]

t2 <- dcast(b2, Cause + Variable ~ DB, value.var = "HR")

t2[, Cause := factor(Cause, levels = c("SCD", "Other"))]
t2 <- t2[order(Cause, Variable)]
t2[1:5, 1:5]

fwrite(t2, "../lodo/betas-fpm-full.csv")
