#!/bin/bash --login

export OMP_NUM_THREADS=4

# Cap on number of variables
# ---------------------------
# Possible options: nocap-var-sel, pre-sel-vars-var-sel
CAP_STR="nocap-var-sel"
# CAP_STR="pre-sel-vars-var-sel"

exec 1> ../logs/${CAP_STR}-devstack-dnn-predict-holdout.log 2>&1

# Input files
# ------------
DT_DIR=../../c-data/data

DT_PATH=${DT_DIR}/"holdout-imputed-inter.csv"

if [ ! -f ${DT_PATH} ]; then
    echo "File ${DT_PATH} not found."
    exit -1
fi

echo -e "Test data file:\n ${DT_PATH}"

echo $(md5sum ${DT_PATH})

# Directory for model files
# --------------------------------
MOD_DIR=../models/${CAP_STR}/devstack

if [ ! -d ${MOD_DIR} ]; then
    echo "Model directory not found."
    exit -1
fi

echo -e "\nModel directory:\n ${MOD_DIR}\n"

# Fixed time predictions files
# ----------------------------
FT_TIMES_PATH=${DT_DIR}/holdout-prediction-times-fixed.csv
FT_OUT_PATH=../predictions/${CAP_STR}/devstack/devstack-holdout-predictions-fixed.csv

if [ ! -f ${FT_TIMES_PATH} ]; then
    echo "File ${FT_TIMES_PATH} not found."
    exit -1
fi

# Do predictions
# --------------------
echo -e "Fixed time predictions: \n${FT_TIMES_PATH} \n${FT_OUT_PATH}\n"

~/rds/profid/spectra/venv-icsf/bin/dnn_predict \
    -d ${DT_PATH} \
    -m ${MOD_DIR} \
    -o ${FT_OUT_PATH} \
    -t ${FT_TIMES_PATH};

# Predictions for IBS
# ----------------------
IBS_TIMES_PATH=${DT_DIR}/holdout-prediction-times-ibs-grid.csv
IBS_OUT_PATH=../predictions/${CAP_STR}/devstack/devstack-holdout-predictions-ibs-grid.csv

if [ ! -f ${IBS_TIMES_PATH} ]; then
    echo "File ${PRED_TIMES_PATH} not found."
    exit -1
fi

echo -e "\n\nIBS grid predictions \n${IBS_TIMES_PATH} \n${IBS_OUT_PATH}\n"

~/rds/profid/spectra/venv-icsf/bin/dnn_predict \
    -d ${DT_PATH} \
    -m ${MOD_DIR} \
    -o ${IBS_OUT_PATH} \
    -t ${IBS_TIMES_PATH};

echo "\n\nDone."
