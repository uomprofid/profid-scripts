# Description
# ==============================================================================
## Calculate AUC at 12 months for LODO analysis using predictions from models
## fit on pre-selected variables.

rm(list = ls())

# Requirements
# ==============================================================================
library(data.table)
library(ggplot2)
library(metamisc)

source("./utils/hz-prediction-performance-measures.R")

# Data
# ==============================================================================
## Outcome data for UOM datastack
dt <- readRDS("../../a-datastack/output/datastack-num-train.rds")
dt <- dt[, .(DB, ID, Status, Survival_time)]
dim(dt)

## Dataset types
icd.sets <- c("CERT", "DOIT", "FREN", "HELS", "ISRL", "MDII",
              "MDRT", "NANC", "OLMC", "PRSE", "SHFT")

mi.sets <- c("ASTN", "ATMS", "DRVT", "ISAR", "PRDT", "SLSN", "SWHR")

# LVEF as a measure of risk of SCD
# ==============================================================================
(lvef.files <- list.files("../../c-data/data/lodo",
                          patter = "test-imputed-inter.csv",
                          full.names = TRUE))

lvef <- list()
for (f in lvef.files) {
  db <- gsub("([a-z]{4})-test-imputed-inter.csv", "\\1", basename(f))
  lvef[[db]] <-
    fread(f)[, .(ID, Time = 12, CIF = (100 - LVEF) / 100)][, DB := toupper(db)]
}
rm(f, db)

lvef <- rbindlist(lvef)

lvef[, Method := "LVEF"]
lvef[, Event := 1]

stopifnot(uniqueN(lvef) == nrow(lvef))

dim(lvef)

# Weibull model predictions
# ==============================================================================
(lf.wbl <- list.files("../../d-fpm/predictions/pre-sel-vars/weibull/lodo",
                      pattern = "test-predictions-shrunk-fixed.csv",
                      full.names = TRUE))

wbl <- list()
for (f in lf.wbl) {
  db <- gsub("([a-z]{4})-.*", "\\1", basename(f))
  wbl[[db]] <- fread(f)[, DB := toupper(db)]
}
rm(f, db)

wbl <- rbindlist(wbl)

wbl[, Method := "Weibull"]
wbl <- wbl[Event == 1]

stopifnot(uniqueN(wbl) == nrow(wbl))

dim(wbl)

# FPM DF2 model predictions
# ==============================================================================
(lf.fpm.df2 <- list.files("../../d-fpm/predictions/pre-sel-vars/fpm-df2/lodo",
                          pattern = "test-predictions-shrunk-fixed.csv",
                          full.names = TRUE))

fpm.df2 <- list()
for (f in lf.fpm.df2) {
  db <- gsub("([a-z]{4})-.*", "\\1", basename(f))
  fpm.df2[[db]] <- fread(f)[, DB := toupper(db)]
}
rm(f, db)

fpm.df2 <- rbindlist(fpm.df2)

fpm.df2[, Method := "FPM"]
fpm.df2 <- fpm.df2[Event == 1]

stopifnot(uniqueN(fpm.df2) == nrow(fpm.df2))

dim(fpm.df2)

# RF predictions
# ==============================================================================
(lf.rf <- list.files("../../d-rf/predictions/pre-sel-vars-var-sel/lodo",
                     pattern = "test-predictions-fixed.csv",
                     full.names = TRUE))

rf <- list()
for (f in lf.rf) {
  db <- gsub("([a-z]{4})-.*", "\\1", basename(f))
  rf[[db]] <- fread(f)[, DB := toupper(db)]
}
rm(f, db)

rf <- rbindlist(rf)

rf[, Method := "RF"]
rf <- rf[Event == 1]
rf[, CHF := NULL]

stopifnot(uniqueN(rf) == nrow(rf))

dim(rf)

# DNN predictions
# ==============================================================================
(lf.dnn <- list.files("../../d-dnn/predictions/pre-sel-vars-var-sel/lodo",
                      pattern = "test-predictions-fixed.csv",
                      full.names = TRUE))

dnn <- list()
for (f in lf.dnn) {
  db <- gsub("([a-z]{4})-.*", "\\1", basename(f))
  dnn[[db]] <- fread(f)[, DB := toupper(db)]
}
rm(f, db)

dnn <- rbindlist(dnn)

dnn[, Method := "DNN"]
dnn <- dnn[Event == 1]
dnn[, CHF := NULL]

stopifnot(uniqueN(dnn) == nrow(dnn))

dim(dnn)

# Predictions stack
# ==============================================================================
pred <- rbind(lvef, wbl, fpm.df2, rf, dnn)
head(pred)
dim(pred)

stopifnot(all(pred$ID %in% dt$ID))

## Select predictions at 12 months
pred <- pred[Time == 12]
dim(pred)

## Predictions by DB
pred[, .N, DB]

## Check DB-ID consistency
pred[, DB_tmp := gsub("([A-Z]{4})_.*", "\\1", ID)]
pred[DB_tmp %in% c("PRSI", "PRSL"), DB_tmp := "PRSE"]
stopifnot(nrow(pred[DB_tmp != DB]) == 0)
pred[, DB_tmp := NULL]

# AUC
# ==============================================================================
dauc <- copy(pred)

## Merge outcome data
dauc <- merge(dauc, dt[, -"DB"], by = "ID")

## Re-define outcomes at 12 months
dauc <- dauc[!(Survival_time < 12 & Status == 0)]
dim(dauc)

dauc[, Status_12 := NA_integer_]
dauc[Survival_time > 12, Status_12 := 0]
dauc[Survival_time <= 12 & Status == 1, Status_12 := 1]
dauc[Survival_time <= 12 & Status == 2, Status_12 := 0]

dauc[, .N, keyby = .(Status, Status_12)]

auc12 <- dauc[, .(auc = auc(roc(Status_12, CIF))[1],
                  var = var(roc(Status_12, CIF))[1]),
              .(Method, DB)]

auc12[, se := sqrt(var)]

head(auc12)

## Check mean AUC
auc12[, .(mean_auc = mean(auc)), Method][order(mean_auc)]

## Add dataset types
auc12[, DB_type := NA_character_]
auc12[DB %in% icd.sets, DB_type := "ICD"]
auc12[DB %in% mi.sets, DB_type := "MI"]
stopifnot(!anyNA(auc12$DB_type))

## Add N
n <- dt[, .N, .(DB = DB)]
auc12 <- merge(auc12, n, by = "DB", all.x = TRUE)
auc12[, DB := paste0(DB, "\n", N)]
auc12 <- auc12[order(auc)]

auc12
dim(auc12)

## ADD WDHR results
wdhr <- fread("../../f-wdhr/send-to-uom/auc12/wdhr-auc-12-pre-sel-vars.csv")
head(wdhr)

(wdhr <- wdhr[, .(Method, DB = LODB, auc, var, se, DB_type = "MI", N)])

auc12 <- rbind(auc12, wdhr)

## 95% CI
auc12[, lci := auc - 1.96 * se]
auc12[, uci := auc + 1.96 * se]
auc12[lci < 0, lci := 0]
auc12[uci > 1, uci := 1]
auc12

head(auc12)
dim(auc12)

## Order dataset names by average C-index across methods
cm <- auc12[, .(mean_method = mean(auc)), .(DB)]
cm <- cm[order(mean_method)]
cm

auc12[, DB := factor(DB, levels = cm$DB)]

## Pooled AUC
cipooled <- auc12[, {
  w <- 1 / se ^ 2
  cw <- auc * w
  list(type = "Pooled AUC", value = sum(cw) / sum(w))
}, Method][order(value)]

cipooled

fwrite(cipooled, "../lodo/lodo-auc-12-pooled-pre-sel-vars.csv")

# Meta analysis
# ==============================================================================
DoMetaAnalysis <- function(est, se) {
  require(metamisc)

  vm <- valmeta(measure = "cstat",
                cstat = est,
                cstat.se = se,
                method = "REML",
                pars = list(model.cstat = "normal/logit"))

  list(mu = vm$est, lci = vm$ci.lb, uci = vm$ci.ub, lpi = vm$pi.lb, upi = vm$pi.ub)
}


## DoMetaAnalysis <- function(est, se, alpha = 0.05) {
##   require(metafor)
##   stopifnot(length(est) == length(se))
##   meta <- rma(est, se ^ 2, method = "REML")
##   mu <- meta$beta[1, 1]
##   tau2 <- meta$tau2
##   mult <- sqrt(tau2 + 1 / sum(1 / (se ^ 2 + tau2)))
##   t <- abs(qt(alpha / 2, length(est) - 2))
##   lpi <- mu - t * mult
##   upi <- mu + t * mult
##   list(mu = mu, lpi = lpi, upi = upi)
## }

meta1 <- auc12[, DoMetaAnalysis(auc, se), .(Method)]
meta1 <- meta1[order(mu)]
meta1

meta1[, `95% CI` := sprintf("(%.3f--%.3f)", lci, uci)]
meta1[, `95% PI` := sprintf("(%.3f--%.3f)", lpi, upi)]

meta1 <- meta1[, .(Method, Mean = round(mu, 3), `95% CI`, `95% PI`)]
meta1

fwrite(meta1, "../lodo/lodo-meta-analysis-pre-sel-vars-by-method.csv")

meta2 <- auc12[, DoMetaAnalysis(auc, se), .(DB_type, Method)]
meta2 <- meta2[order(DB_type, mu)]
meta2

meta2[, `95% CI` := sprintf("(%.3f--%.3f)", lci, uci)]
meta2[, `95% PI` := sprintf("(%.3f--%.3f)", lpi, upi)]

meta2 <- meta2[, .(Type = DB_type, Method, Mean = round(mu, 3), `95% CI`, `95% PI`)]
meta2

fwrite(meta2, "../lodo/lodo-meta-analysis-pre-sel-vars-by-method-by-db-type.csv")

# LODO plot
# ==============================================================================
m1 <- meta1[Method == "LVEF"]

auc12[, Method := factor(Method, levels = c("LVEF", "DNN", "RF", "FPM", "Weibull"))]

## cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

cbPalette <- c("#999999", "#E69F00", "#009E73", "#0072B2", "#CC79A7")

p1 <- ggplot(auc12, aes(x = DB, y = auc)) +
  geom_point(size = 2, aes(col = Method),
             position = position_dodge(0.7)) +
  geom_errorbar(aes(ymin = lci, ymax = uci, col = Method),
                position = position_dodge(0.7), show.legend = FALSE) +
  geom_hline(yintercept = 0.5, linetype = 2) +
  ## geom_hline(data = m1, aes(yintercept = mu, col = Method), linetype = 1) +
  ## geom_hline(data = m1, aes(yintercept = lpi, col = Method), linetype = 2) +
  ## geom_hline(data = m1, aes(yintercept = upi, col = Method), linetype = 2) +
  ylab("AUC at 12 months (95% CI)") + xlab("Test dataset (N)") +
  scale_y_continuous(breaks = seq(0.30, 1, 0.10)) +
  scale_color_manual(name = element_blank(), values = cbPalette) +
  theme_bw() +
  theme(text = element_text(size = 14, colour = "black"),
        axis.text = element_text(colour = "black"),
        legend.text = element_text(size = 14),
        legend.position = "top")
p1 <- p1 + coord_flip()
## p1

ggsave("../lodo/lodo-auc-12-plot-pre-sel-vars.tiff", p1,
       width = 27, height = 21, units = "cm", dpi = 300)

# Predicted probabilities
# ==============================================================================
pp <- pred[Method != "LVEF"]

pp[, .N, Method]

pp.plt <- ggplot(pp) +
  geom_histogram(aes(x = CIF, y = (..count..)), bins = 30, fill = "gray60") +
  facet_wrap( ~ Method) +
  xlab("Probability of SCD (proxy) within 12 months") + ylab("Count") +
  scale_x_continuous(limits = c(0, 0.10)) +
  theme_bw() +
  theme(text = element_text(size = 14, colour = "black"),
        axis.text = element_text(colour = "black"))
pp.plt

ggsave("../lodo/lodo-12-month-probs.tiff", pp.plt,
       width = 27, height = 21, units = "cm", dpi = 300)

## Above 2.5% and 3%
cif.n <- pp[, .(`Above 2.5% N` = sum(CIF > 0.025),
                `Above 3% N` = sum(CIF > 0.03),
                `Above 5% N` = sum(CIF > 0.05),
                N = .N), Method]

cif.n[, `Above 2.5% prop.` := `Above 2.5% N` / N]
cif.n[, `Above 3% prop.` := `Above 3% N` / N]
cif.n[, `Above 5% prop.` := `Above 5% N` / N]
cif.n

fwrite(cif.n, "../lodo/lodo-12-month-probs-summary.csv")
