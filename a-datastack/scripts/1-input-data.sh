#!/bin/bash
# Fetch input files from datasets directories. Input files should contain data
# standardised to the PROFID common data model.

# File pattern
PATTERN="*/data/processed/*-common-data-model.rds"

# Search for data files in this direcotry
DATASETS_DIR=~/rds/profid/datasets/local

# Input files directory
CDM_DIR=../input

# Log file
LOG_FILE=../logs/$(basename "$0").log

# Clear input files directory
rm -v ${CDM_DIR}/* > ${LOG_FILE} 2> /dev/null

# Find and copy processed data files
find ${DATASETS_DIR} -type f -wholename ${PATTERN} \
     -exec cp -v {} ${CDM_DIR} >> ${LOG_FILE} 2>&1 \;

# Print error log if the last command exited with error
EC="$?"
if [ ${EC} -ne 0 ]; then
    cat ${LOG_FILE}
    exit ${EC}
fi

# Log checksums
echo -e "\nSHA256 checksums:" >> ${LOG_FILE}
sha256sum ${CDM_DIR}/* >> ${LOG_FILE} 2>&1
