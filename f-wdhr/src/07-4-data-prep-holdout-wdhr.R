# Description
# ==============================================================================
## Prepare holdout stack.

rm(list = ls())

options(width = 112, length = 99999)

Sys.setenv(LANG = "en")
Sys.setlocale("LC_ALL", "English")

# Requirements
# ==============================================================================
library(data.table)
library(splines)
library(tools)

source("./hz-interactions-and-splines.R")

# Interactions
# ==============================================================================
interactions <- as.formula(~ Age:LVEF +
                             Age:eGFR_log1p +
                             Age:NTProBNP_log1p +
                             NTProBNP_log1p:Sex_BIN_Male)
# Data
# ==============================================================================
dt <- readRDS("../data/processed/wdhr-num-holdout.rds")
dim(dt)

# Data preparation
# ==============================================================================

# Imputed data
# ---------------------------------------------------------------------------
## Holdout data
holdout.path.imp <- "../data/processed/holdout-imputed.csv"
holdout.imp <- fread(holdout.path.imp)

setcolorder(holdout.imp, c("ID", "Status", "Survival_time"))

stopifnot(!anyNA(holdout.imp))
stopifnot(all(holdout.imp$ID %in% dt$ID))

cat("\nHoldout dim:", dim(holdout.imp), "\n")
print(md5sum(holdout.path.imp))

# Interactions
# ---------------------------------------------------------------------------
## Holdout data
holdout.imp.inter <- HZ.interactions_add(holdout.imp, interactions)
dim(holdout.imp.inter)

# Drop variables with no variance
train <- fread("../data/processed/wdhr-train-imputed-inter.csv")

holdout.imp.inter <- holdout.imp.inter[, colnames(train), with = FALSE]
dim(holdout.imp.inter)

rm(train)

holdout.imp.inter.path <- "../data/processed/wdhr-holdout-imputed-inter.csv"
fwrite(holdout.imp.inter, holdout.imp.inter.path)

# Round imputed binary values for random forests
# ---------------------------------------------------------------------------
## Use only the dataset with interactions
holdout.imp.inter.rf <- copy(holdout.imp.inter)

for (x in colnames(holdout.imp.inter.rf)) {
  if (grepl("_BIN_", x)) {
    stopifnot(all(dt[[x]] %in% c(NA, 0, 1)))
    ## Holdouting data
    holdout.imp.inter.rf[get(x) < 0, (x) := 0]
    holdout.imp.inter.rf[get(x) > 1, (x) := 1]
    ## Note: exact 0.5 will be rounded to 0
    holdout.imp.inter.rf[get(x) >= 0 & get(x) <= 1, (x) := round(get(x))]
  }
}

holdout.imp.inter.rf.path <- "../data/processed/wdhr-holdout-imputed-inter-rf.csv"
fwrite(holdout.imp.inter.rf, holdout.imp.inter.rf.path)
