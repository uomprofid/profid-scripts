clear all

// Set number of cores
set processors 4

// Command line arguments
args data_file model_file  output_file
display "`data_file'"
display "`model_file'"
display "`output_file'"

// Read the data file
insheet using "`data_file'"

// Set the survival time variables
estimates use "`model_file'"

ereturn list

generate _status = .

// Generate spline functions
generate ln_time = ln(time)
matrix m1 = e(R_bh_c1)

if e(dfbase_c1) == "1" {
	tokenize `e(boundary_knots_c1)'
	local bk1=ln(`1')
	local bk2=ln(`2')
	local lnbknots="`bk1' `bk2'"
	rcsgen ln_time, knots("`lnbknots'") gen(_rcs_c1_) dgen(_d_rcs_c1_) rmatrix(m1)	
}
else {
	rcsgen ln_time, knots(`e(ln_bhknots_c1)') gen(_rcs_c1_) dgen(_d_rcs_c1_) rmatrix(m1)
}

outsheet id time time_id ln_time _rcs_c1_* _d_rcs_c1_* ///
    using "`output_file'", comma replace
