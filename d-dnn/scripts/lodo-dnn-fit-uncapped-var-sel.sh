#!/bin/bash --login
#$ -cwd
#$ -o /dev/null
#$ -e /dev/null
#$ -pe smp.pe 8
#$ -t 1:18

export OMP_NUM_THREADS=${NSLOTS}

module load apps/gcc/R/3.6.1

exec 1> ../logs/nocap-var-sel-lodo-dnn-fit-uncapped-var-sel-${SGE_TASK_ID}.log 2>&1

# Input files
# ------------
DT_DIR=../../c-data/data

DB=$(sed -n "${SGE_TASK_ID}"p ${DT_DIR}/db-names-lodo.csv)

DT_PATH=${DT_DIR}/lodo/$DB"-train-imputed-inter.csv"

if [ ! -f ${DT_PATH} ]; then
 echo "File ${DT_PATH} not found."
 exit -1
fi

echo -e "Training data file:\n ${DT_PATH}\n"

echo $(md5sum ${DT_PATH})

# Output directory for model files
# --------------------------------
MOD_DIR=../models/nocap-var-sel/lodo/${DB}

if [ -d ${MOD_DIR} ]; then
    rm ${MOD_DIR}/*.*
else
    mkdir -p ${MOD_DIR}
fi

echo -e "\nModel directory:\n ${MOD_DIR}\n"

# Run DNN model fitter
# --------------------
echo -e "Fitting a model ...\n\n"

~/rds/profid/spectra/venv-csf/bin/dnn_fit -d ${DT_PATH} \
                                          -o ${MOD_DIR} \
                                          -v -1 \
                                          -s 1;

echo "\n\nDone."
