rm(list = ls())

options(width=116, length=99999)

# Requirements
# ==============================================================================
## Use this R module:
## module load apps/gcc/R/4.0.2-gcc830

library(data.table)
library(openxlsx)

source("./utils/hz-basic-summary-statistics.R")

# CDM
# ==============================================================================
cdm <- fread("../../a-datastack/etc/profid-common-data-model.csv")
dim(cdm)

cdm[`Variable name` == "Diseased_arteries_num",
    `Variable name` := "Diseased_arteries_num_cat"]

cdm <- cdm[!(`Variable name` %in% c("HR_average", "SDNN"))]

# UOM datastack
# ==============================================================================
dt <- readRDS("../../a-datastack/output/datastack.rds")
dim(dt)

train <- readRDS("../../a-datastack/output/datastack-num-train.rds")
dim(train)

## Holdout data
hld <- readRDS("../../a-datastack/output/datastack-num-holdout.rds")
dim(hld)

dt.train <- dt[ID %in% train$ID]
dim(dt.train)

dt.hld <- dt[ID %in% hld$ID]
dim(dt.hld)

stopifnot(nrow(dt.train) == nrow(train))
stopifnot(nrow(dt.hld) == nrow(hld))

# UOM data summaries
# ==============================================================================
dt.total.sm <- HZ.eda_describe_data(dt)$summary.sdc
dt.train.sm <- HZ.eda_describe_data(dt.train)$summary.sdc
dt.hld.sm <- HZ.eda_describe_data(dt.hld)$summary.sdc

# WDHR data summaries
# ==============================================================================
wdhr.total.sm <-
  read.xlsx("../../f-wdhr/send-to-uom/tables/table-1/basic-data-summaries-total.xlsx")
wdhr.total.sm <- as.data.table(wdhr.total.sm)
head(wdhr.total.sm)

wdhr.train.sm <-
  read.xlsx("../../f-wdhr/send-to-uom/tables/table-1/basic-data-summaries-train.xlsx")
wdhr.train.sm <- as.data.table(wdhr.train.sm)
head(wdhr.train.sm)

wdhr.hld.sm <-
  read.xlsx("../../f-wdhr/send-to-uom/tables/table-1/basic-data-summaries-holdout.xlsx")
wdhr.hld.sm <- as.data.table(wdhr.hld.sm)
head(wdhr.hld.sm)

# Add variable columns
# ==============================================================================
AddVarCol <- function(d.) {
  d <- copy(d.)

  d[, Variable := NA_character_]
  d[!grepl("^ +", Category), Variable := Category]

  for (i in 1:nrow(d)) {
    if (is.na(d[i, Variable])) {
      d[i, Variable := d[i - 1, Variable]]
    }
  }

  d[, Variable := gsub("^ +| +$", "", Variable)]
  d[, Category := gsub("^ +| +$", "", Category)]

  setcolorder(d, "Variable")

  d
}

wdhr.total.sm <- AddVarCol(wdhr.total.sm)
wdhr.train.sm <- AddVarCol(wdhr.train.sm)
wdhr.hld.sm <- AddVarCol(wdhr.hld.sm)

## unique(wdhr.total$Category)

# Unpack summaries
# ==============================================================================
Unpack <- function(d.) {
  d <- copy(d.)

  d[, Mean := NA_real_]
  d[grepl("Mean \\(SD\\)", Category),
    Mean := as.numeric(gsub("([0-9.]+) .+", "\\1", Summary))]

  d[, SD := NA_real_]
  d[grepl("Mean \\(SD\\)", Category),
    SD := as.numeric(gsub("[0-9.]+ \\(([0-9.]+)\\)", "\\1", Summary))]

  d[, Median := NA_real_]
  d[grepl("Median \\(IQR\\)", Category),
    Median := as.numeric(gsub("([0-9.]+) .+", "\\1", Summary))]

  d[, Count := NA_real_]
  d[grepl("[0-9.]+%", Summary),
    Count := as.numeric(gsub("([0-9.]+) .+", "\\1", Summary))]

  d[]
}

dt.total.sm.raw <- Unpack(dt.total.sm)
dt.train.sm.raw <- Unpack(dt.train.sm)
dt.hld.sm.raw <- Unpack(dt.hld.sm)

wdhr.total.sm.raw <- Unpack(wdhr.total.sm)
wdhr.train.sm.raw <- Unpack(wdhr.train.sm)
wdhr.hld.sm.raw <- Unpack(wdhr.hld.sm)

## Save for CDM plot
## fwrite(wdhr.total.sm.raw, "../../f-wdhr/send-to-uom/tables/table-1/cdm-total.csv")

# Datastack
# ==============================================================================
stopifnot(identical(dt.total.sm$Variable, dt.train.sm$Variable))
stopifnot(identical(dt.total.sm$Variable, dt.hld.sm$Variable))

uom1 <- merge(dt.train.sm, dt.hld.sm,
              by = c("Variable", "Category"),
              suffixes = c("_uom_train", "_uom_holdout"), sort = FALSE)
head(uom1)

wdhr1 <- merge(wdhr.train.sm, wdhr.hld.sm, by = c("Variable", "Category"),
               suffixes = c("_wdhr_train", "_wdhr_holdout"), sort = FALSE)
head(wdhr1)

# Total column
# ==============================================================================
# Mean variables
# ------------------------------------------------------------------------------
mean.vars <- c("Age", "BMI", "SBP", "DBP", "LVEF")

## Add N column
dt.total.sm.raw[, N := as.integer(dt.total.sm.raw[Variable == "N", Summary])]
wdhr.total.sm.raw[, N := as.integer(wdhr.total.sm.raw[Variable == "N", Summary])]

total <- merge(dt.total.sm.raw, wdhr.total.sm.raw,
               by = c("Variable", "Category"), sort = FALSE, all.x = TRUE,
               suffixes = c("_uom", "_wdhr"))
head(total)
dim(total)

total[, N_total := N_uom + N_wdhr]

total[, Weight_uom := N_uom / N_total]
total[, Weight_wdhr := N_wdhr / N_total]

total[grepl("Mean", Category) & Variable %in% mean.vars,
      Mean := Mean_uom * Weight_uom + Mean_wdhr * Weight_wdhr]

total[grepl("Mean", Category) & Variable %in% mean.vars,
      SD := sqrt(SD_uom ^ 2 * Weight_uom ^ 2 + SD_wdhr ^ 2 * Weight_wdhr ^ 2)]

total[grepl("Mean", Category), Summary := sprintf("%.1f (%.1f)", Mean, SD)]

# Median variables
# ------------------------------------------------------------------------------
## Median variables: for logged variables show medians
median.vars <- c(
  ## "BMI", show mean for BMI
  "LVDD",
  "BUN",
  "Cholesterol",
  "CRP",
  "eGFR",
  "Haemoglobin",
  "HbA1c",
  "HDL",
  "IL6",
  "LDL",
  "NTProBNP",
  "Potassium",
  "Sodium",
  "Triglycerides",
  "Troponin_T",
  "TSH",
  "HR",
  "PR",
  "QRS",
  "QTc",
  "Time_index_MI_CHD"
)

uom.qf <- readRDS("../tables/uom-total-quantile-fun.rds")
names(uom.qf)

wdhr.qf <- readRDS("../../f-wdhr/send-to-uom/tables/table-1/total-quantile-fun.rds")
names(wdhr.qf)

x.med.wdhr <- names(wdhr.qf)[names(wdhr.qf) %in% median.vars]
x.med.wdhr

(n1 <- dt.total.sm.raw$N[1])
(n2 <- wdhr.total.sm.raw$N[1])
(n <- n1 + n2)

## eGFR
qf1 <- wdhr.qf[["eGFR"]]
qf2 <- uom.qf[["eGFR"]]
(r <- range(dt[["eGFR"]], na.rm = TRUE))

f.med <- function(x, q = 1/2) {
  qf1$Quantile(n / n1 * (q - n2 / n * qf2$F(x))) - x
}

(egfr.med <- uniroot(f.med, r, tol = 1e-12)$root)
(egfr.lq <- uniroot(f.med, q = 0.25, r, tol = 1e-12)$root)
(egfr.uq <- uniroot(f.med, q = 0.75, r, tol = 1e-12)$root)

total[Variable == "eGFR" & Category == "Median (IQR)",
      Summary := sprintf("%.1f (%.1f--%.1f)", egfr.med, egfr.lq, egfr.uq)]

rm(qf1, qf2, r, f.med)

## Survival_time
qf1 <- wdhr.qf[["Survival_time"]]
qf2 <- uom.qf[["Survival_time"]]
(r <- range(dt[["Survival_time"]], na.rm = TRUE))

f.med <- function(x, q = 1/2) {
  qf1$Quantile(n / n1 * (q - n2 / n * qf2$F(x))) - x
}

(srvt.med <- uniroot(f.med, r, tol = 1e-12)$root)
(srvt.lq <- uniroot(f.med, q = 0.25, r, tol = 1e-12)$root)
(srvt.uq <- uniroot(f.med, q = 0.75, r, tol = 1e-12)$root)

total[Variable == "Survival_time" & Category == "Median (IQR)",
      Summary := sprintf("%.1f (%.1f--%.1f)", srvt.med, srvt.lq, srvt.uq)]

rm(qf1, qf2, r, n1, n2, n)

# Counts
# ------------------------------------------------------------------------------
total[!is.na(Count_uom) & !is.na(Count_wdhr), Count := Count_uom + Count_wdhr]
total[!is.na(Count_uom) & !is.na(Count_wdhr), Proportion := Count / (N_uom + N_wdhr)]

total[!is.na(Count_uom) & !is.na(Count_wdhr),
      Summary := sprintf("%d (%.1f%s)", Count, 100 * Proportion, "%")]

# Wrap-up the subsection
# ------------------------------------------------------------------------------
total[Variable == "N", Summary := N_total]

total[!(Variable %in% wdhr.total.sm$Variable), Summary := Summary_uom]

total <- total[, .(Variable, Category, Summary_total = Summary)]

# Table 1
# ==============================================================================
dim(uom1)
dim(wdhr1)

wdhr1[!(Variable %in% uom1$Variable), Variable]

wdhr1 <- wdhr1[Variable == "N" | Variable != Category]
wdhr1[!(Category %in% uom1$Category), Category]

table1 <- merge(uom1, wdhr1, by = c("Variable", "Category"),
                all = TRUE, sort = FALSE)
dim(table1)

table1 <- merge(total, table1, by = c("Variable", "Category"),
                all = TRUE, sort = FALSE)
dim(table1)

## Drop some variables
drop.var <- c("ID", "DB", "IsSWHR", "HasMRI", "Time_zero_Ym")

table1 <- table1[!(Variable %in% drop.var)]
dim(table1)

## Add variable classification
table1 <- merge(cdm[, .(Classification, Variable = `Variable name`)],
                table1, by = "Variable", all.y = TRUE)
setcolorder(table1, "Classification")

table1[Variable %in% c("Status", "Survival_time"), Classification := "Outcome"]
table1[Variable %in% c("Endpoint_type", "Baseline_type"), Classification := "Auxiliary"]
table1[Variable == "CVD_risk_region", Classification := "WHO"]

## Order based on CDM variable order
unique(table1[!(Variable %in% cdm[, `Variable name`]), Variable])

var.order <- c("N", "Status", "Survival_time", "Endpoint_type", "Baseline_type",
               cdm[, `Variable name`], "CVD_risk_region")

var.order[!(var.order %in% table1$Variable)]
unique(table1[!(Variable %in% var.order), Variable])

stopifnot(identical(sort(unique(table1$Variable)), sort(var.order)))

table1[, Variable := factor(Variable, levels = var.order)]
table1 <- table1[order(Variable)]

table1 <- table1[!(Variable %in% median.vars & grepl("Mean", Category))]
table1 <- table1[!(Variable %in% mean.vars & grepl("Median", Category))]

table1 <- table1[!(Variable == "Survival_time" & grepl("Mean", Category))]

## Drop Females row
table1 <- table1[Category != "Female"]

## Keep "Yes" only
table1 <- table1[Category != "No"]

## Re-map Status
table1[Variable == "Status" & Category == "0", Category := "Censored"]
table1[Variable == "Status" & Category == "1", Category := "SCD (proxy)"]
table1[Variable == "Status" & Category == "2", Category := "Death other"]

## Re-map CVD risk regions
table1[Variable == "CVD_risk_region" & Category == "1", Category := "Low"]
table1[Variable == "CVD_risk_region" & Category == "2", Category := "Moderate"]
table1[Variable == "CVD_risk_region" & Category == "3", Category := "High"]
table1[Variable == "CVD_risk_region" & Category == "4", Category := "Very high"]

## WARNING: manual fix of CVD_risk_region row for WDHR and Total
table1[grepl("CVD_risk_region", Variable)]

table1[Variable == "CVD_risk_region" & Category == "Low",
       Summary_wdhr_train := "23763 (90%)"]

table1[Variable == "CVD_risk_region" & Category == "Low",
       Summary_wdhr_holdout := "2641 (10%)"]

table1[Variable == "CVD_risk_region" & Category == "Missing",
       c("Summary_wdhr_train", "Summary_wdhr_holdout") := "0 (0%)"]

table1[Variable == "CVD_risk_region" & Category == "Low",
       Summary_total := "31399 (14%)"]

## WARNING: manual fix of Time_index_MI_CHD in WDHR
table1[Variable == "Time_index_MI_CHD" & Category == "Median (IQR)",
       c("Summary_wdhr_train", "Summary_wdhr_holdout") := "1.3 (1.3--1.3)"]

table1[Variable == "Time_index_MI_CHD" & Category == "Missing",
       c("Summary_wdhr_train", "Summary_wdhr_holdout") := "0 (0%)"]

## Rename train column
setnames(table1, "Summary_total", "Total")
setnames(table1, gsub("train", "development", names(table1)))

print(table1, nrow = Inf)
dim(table1)

head(table1)

tmis <- table1[Category == "Missing",
               .(Classification, Variable, Missing_total = Total)]
head(tmis)
dim(tmis)

t1 <- table1[Category != "Missing"]
dim(t1)

t1 <- merge(t1, tmis, by = c("Classification", "Variable"), sort = FALSE)
dim(t1)

t1 <- rbind(t1, table1[Variable == "N"], fill = TRUE)
dim(t1)

setcolorder(t1, c("Classification", "Variable", "Category", "Total", "Missing_total"))
head(t1)

# Drop variables not used in LODO
t1 <- t1[Variable != "Dementia"]
t1 <- t1[Variable != "AV_block_II_or_III"]
dim(t1)

print(t1, nrow = Inf)
dim(t1)

# Output
# ==============================================================================
fwrite(t1, "../tables/table-1-uom-wdhr.csv", na = "-")
