// Fit a competing risks flexible parametic model
clear all

// Set number of cores
set processors 4

// Command line arguments
args data_file model_file df maxtime
display "`data_file'"
display "`model_file'"
display "`df'"
display "`maxtime'"

// Read the data file
insheet using "`data_file'"

// Set the survival time variables
stset survival_time, failure(status == 1, 2) exit(time `maxtime')

// Fit FPM model
stpm2cr [SCD: , scale(hazard) df(`df')] ///
        [Other: , scale(hazard) df(2)] ///
        , events(status) cause(1 2) censvalue(0) eform difficult technique(nr 10 bfgs 10) iterate(500)

// Save log-likelihood value
file open llfile using "`model_file'-loglik.csv", write replace
file write llfile "loglik" _n "`e(ll)'"
file close llfile 

// Save coefficients into an Excel file
putexcel set "`model_file'-betas", replace sheet("betas")
putexcel A1=matrix(e(b)'), rownames

// Save knot locations: SCD
if `df' == 1 {
    // On original scale
    file open knots_file_scd using "`model_file'-boundary-knots-scd.txt", write replace
    file write knots_file_scd "`e(boundary_knots_c1)'"
}
else {
    // On logarithmic scale
    file open knots_file_scd using "`model_file'-ln-knots-scd.txt", write replace
    file write knots_file_scd "`e(ln_bhknots_c1)'"
}
file close knots_file_scd 

// Save knot locations on logarithmic scale: death other
file open knots_file_death_other using "`model_file'-ln-knots-death-other.txt", write replace
file write knots_file_death_other "`e(ln_bhknots_c2)'"
file close knots_file_death_other 

// Save orthogonalisation matrix for baseline hazard functions
putexcel set "`model_file'-bh-orthog-matrix-scd", replace sheet("bh_orthog")
putexcel A1=matrix(e(R_bh_c1)), rownames

putexcel set "`model_file'-bh-orthog-matrix-death-other", replace sheet("bh_orthog")
putexcel A1=matrix(e(R_bh_c2)), rownames

// Log data summaries
estat summarize
// Log model estimates
ereturn list
// Log raw betas
estimates table
// Log exponentiated beta summaries
esttab, b eform p nostar

// Save the model into a file
estimates save "`model_file'.ster", replace
