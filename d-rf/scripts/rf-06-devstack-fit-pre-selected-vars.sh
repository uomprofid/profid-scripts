#!/bin/bash --login
#$ -cwd
#$ -o /dev/null
#$ -e /dev/null
#$ -pe smp.pe 16
#$ -t 1

export OMP_NUM_THREADS=${NSLOTS}

module load apps/gcc/R/3.6.1

R_FILE=./rf-06-devstack-fit-pre-selected-vars.R

LOG_FILE=../logs/rf-06-devstack-fit-pre-selected-vars-${VARIABLE_NUM_CAP}-${SGE_TASK_ID}.Rout
[ -f ${LOG_FILE} ] && rm ${LOG_FILE}

R CMD BATCH --vanilla ${R_FILE} ${LOG_FILE}
