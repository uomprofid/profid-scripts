clear all

// Command line arguments
args data_file model_file  output_file
display "`data_file'"
display "`model_file'"
display "`output_file'"

// Read the data file
insheet using "`data_file'"

// Set the survival time variables
estimates use "`model_file'"

generate _status = .

capture predict cif, cif ci timevar(time)

if _rc != 0 {
    predict cif, cif timevar(time)
    generate cif_c1_lci = .
    generate cif_c1_uci = .
    generate cif_c2_lci = .
    generate cif_c2_uci = .
}

outsheet id time cif_c1 cif_c1_lci cif_c1_uci cif_c2 cif_c2_lci cif_c2_uci ///
    using "`output_file'", comma replace
