rm(list = ls())

Sys.setenv(LANG = "en")
Sys.setlocale("LC_ALL", "English")

library(data.table)

source("./hz-prediction-performance-measures.R")

## Outcome data
dt <- readRDS("../data/processed/wdhr-num-holdout.rds")
dt <- dt[, .(DB, ID, Status, Survival_time, LVEF)]
dim(dt)

## LVEF
lvef <- dt[, .(LODB = "WDHR", Method = "LVEF", Event = 1, ID, ICIF = (100 - LVEF) / 100)]

## Weibull
pred.wbl <- fread("../data/predictions/fpm/uom/weibull/holdout-predictions-shrunk-ibs.csv")
pred.wbl[, Method := "Weibull"]
pred.wbl[, DB := "WDHR"]
stopifnot(uniqueN(pred.wbl) == nrow(pred.wbl))

## FPM DF2
pred.fpm.df2 <- fread("../data/predictions/fpm/uom/fpm-df2/holdout-predictions-shrunk-ibs.csv")
pred.fpm.df2[, Method := "FPM"]
pred.fpm.df2[, DB := "WDHR"]
stopifnot(uniqueN(pred.fpm.df2) == nrow(pred.fpm.df2))

## DNN
pred.dnn <- fread("../data/predictions/dnn/pre-sel-vars/wdhr-holdout-predictions-ibs.csv")
pred.dnn[, Method := "DNN"]
pred.dnn[, DB := "WDHR"]
stopifnot(uniqueN(pred.dnn) == nrow(pred.dnn))

## Stack predictions
pred <- rbind(pred.wbl[Event == 1, .(Method, LODB = DB, ID, Event, Time, CIF)],
              pred.fpm.df2[Event == 1, .(Method, LODB = DB, ID, Event, Time, CIF)],
              pred.dnn[Event == 1, .(Method, LODB = DB, ID, Event, Time, CIF)])
dim(pred)

all(pred$ID %in% dt$ID)

pred[, ID := as.character(ID)]

## Integrate CIF
(icif <- pred[, .(ICIF = HZ.ppm_integrate_cif(CIF, Time)), .(Method, LODB, Event, ID)])

## Add LVEF
icif <- rbind(icif, lvef)
dim(icif)

icif <- merge(icif, dt[, .(ID, Status, Survival_time)], by = "ID", sort = FALSE)
dim(icif)

max.time <- max(dt$Survival_time)
max.time

icif[, Status_Cstat := NA_integer_]
icif[Status == 0, Status_Cstat := 0]
icif[Status == 1, Status_Cstat := 1]
icif[Status == 2, Status_Cstat := 0]
icif[Status == 2, Survival_time := max.time]

icif[, .N, keyby = .(Status, Status_Cstat)]

(cstat <- icif[, HZ.ppm_c_index(Survival_time, Status_Cstat, ICIF), .(Method, LODB, Event)])

fwrite(cstat, "../send-to-uom/cstat/wdhr-c-stat-pre-sel-vars.csv")
